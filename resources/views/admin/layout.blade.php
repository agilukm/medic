<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Rekam Medis</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{url('/')}}/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="{{url('/')}}/admin/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="{{url('/')}}/admin/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{url('/')}}/admin/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{url('/')}}/admin/images/favicon.png" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link href="{{url('/')}}/admin/css/lib/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">

      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-right">
          <!-- <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">4</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">
                </p>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Stok Obat</h6>
                  <p class="font-weight-light small-text">
                    Just now
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="mdi mdi-comment-text-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Pemeriksaan</h6>
                  <p class="font-weight-light small-text">
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="mdi mdi-email-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Pembayaran</h6>
                  <p class="font-weight-light small-text">
                  </p>
                </div>
              </a>
            </div>
          </li> -->
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Halo, {{ Auth::user()->name }} !</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <!-- <a class="dropdown-item">
                Change Password -->
              </a>
              <a class="dropdown-item" href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                  {{ __('Keluar') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <p class="profile-name">{{ Auth::user()->name }}</p>
                  <div>
                    <small class="designation text-muted">
                        @if(Auth::user()->roles == 1)
                            Admin
                        @elseif(Auth::user()->roles == 2)
                            Bidan
                        @else
                            Pemilik
                        @endif
                    </small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>

            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/pendaftaran')}}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Pendaftaran</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Pemeriksaan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/perawatan/Imunisasi') }}">Imunisasi</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/perawatan/Kandungan') }}">Kandungan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/perawatan/Lahiran') }}">Lahiran</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/perawatan/Kb') }}">KB</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/rekam_medis')}}">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">Rekam Medis</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basicobat" aria-expanded="false" aria-controls="ui-basicobat">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Obat</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basicobat">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/obat') }}">Data Obat</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/obat/pemasukan') }}">Pemasukan Obat</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/obat/pengeluaran') }}">Pengeluaran Obat</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/obat/penggunaan') }}">Penggunaan Pemeriksaan Obat</a>
                </li>
              </ul>
            </div>
          </li>
          @if(Auth::user()->roles == 1)
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basicmaster" aria-expanded="false" aria-controls="ui-basicmaster">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Master</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basicmaster">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/master/biaya') }}">Data Biaya Pemeriksaan</a>
              </li>
              </ul>
            </div>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/pembayaran') }}">
              <i class="menu-icon mdi mdi-table"></i>
              <span class="menu-title">Pembayaran</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/laporan') }}">
              <i class="menu-icon mdi mdi-sticker"></i>
              <span class="menu-title">Laporan</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
      @yield('content')
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->

    <script src="{{ url('/')}}/admin/js/lib/jquery/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.4/metisMenu.min.js" integrity="sha256-YqTSp9JlAIbtktRVVwWzOEXJAIs8vE/r79vR7jcvYj0=" crossorigin="anonymous"></script>

  <script src="{{url('/')}}/admin/vendors/js/vendor.bundle.base.js"></script>
  <script src="{{url('/')}}/admin/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{url('/')}}/admin/js/off-canvas.js"></script>
  <script src="{{url('/')}}/admin/js/misc.js"></script>
  <script src="{{url('/')}}/admin/moment.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{url('/')}}/admin/js/dashboard.js"></script>

    <script src="{{ url('/')}}/admin/js/lib/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

  <!-- End custom js for this page-->
  @yield('plugin')
  <script type="text/javascript">
    function format_number(n, currency) {
        return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
        });
    }

    $(document).ready( function () {



        var table = $('#myTable').DataTable();
        $('.select2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });
        // $('#myTable tbody').on( 'click', 'tr', function () {
        //     if ( $(this).hasClass('selected') ) {
        //         $(this).removeClass('selected');
        //     }
        //     else {
        //         table.$('tr.selected').removeClass('selected');
        //         $(this).addClass('selected');
        //     }
        // });

        $('.conf').click( function() {
            var conf = confirm(this.value, this.id, this.name);
        });

        function confirm(uri, id, name){
            swal({
                title: "Data akan di"+name,
                text: "Apakah anda yakin?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yakin",
                cancelButtonText: "Batal",
                confirmButtonColor: "#ec6c62"
            }, function (isConfirm) {
                if (!isConfirm) return;
                table.row($('.'+id)).remove().draw( false );
                    $.ajax({
                        url: uri,
                        type: "GET",
                        dataType: "html",
                        success: function () {
                            var asd = table.row($('.'+id)).remove().draw( false );
                            if(asd)
                            {
                                swal("Behasil!", "Data berhasil di"+name, "success");
                            }
                        },

                        error: function (xhr, ajaxOptions, thrownError) {
                                swal("Behasil!", "Data berhasil di"+name, "success");
                        }
                    });
            });
        }
    });
    </script>

    @if(session()->has('message'))
    <script type="text/javascript">
        swal("Behasil!", "{{ session()->get('message') }}", "success", 3000, false);
    </script>
    @endif

    @if($errors->any())
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#errormodal').modal('show');
        });
    </script>
    @endif


</body>

</html>
