@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pembayaran</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Pendaftaran</th>
                  <th>Nama Pasien</th>
                  <th>Jumlah</th>
                  <th>Diskon</th>
                  <th>Total</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->pendaftaran->kode_registrasi}}</td>
                  <td>{{$da->pendaftaran->pasien->nama_pasien}}</td>
                  <td>Rp {{number_format($da->jumlah, 2, ',', '.')}}</td>
                  <td>{{$da->diskon}} %</td>
                  <td>Rp {{number_format($da->total, 2, ',', '.')}}</td>
                  <td>{{$da->created_at->format('d-m-Y')}}</td>
                  <td>
                      <a href="{{ url('/pembayaran/detail/'.$da->pendaftaran->id)}}" class="btn btn-info">Detail</a> <br>
                      <a onclick="printExternal('{{ url('/') }}/pembayaran/detail/{{$da->pendaftaran->id}}')" href="#" class="btn btn-success">Cetak</a>
                  </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                            <th>No</th>
                            <th>Nama Jasa</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama Jasa</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
<script type="text/javascript">
function printExternal(url) {
var printWindow = window.open( url, 'Print', 'left=200, top=200, width=950, height=500, toolbar=0, resizable=0');
printWindow.addEventListener('load', function(){
    printWindow.print();
    printWindow.close();
}, true);
}
</script>
@endsection
