@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
      <div class="col-lg-12">
                  <div class="card px-2">
                    <div class="card-body">
                        <form method="post" action="{{ url('/pembayaran/bayar')}}">
                      <div class="container-fluid">
                          <center></center>
                        <h3 class="text-right my-5">{{$registrasi->kode_registrasi}}</h3>
                        <hr> </div>
                      <div class="container-fluid d-flex justify-content-between">
                        <div class="col-lg-3 pl-0">
                          <p class="mt-5 mb-2" style="font-size: 21px">
                            <b>Bidan Kusniah. S,ST</b>
                          </p>
                          <p>SIPB : 446-4/2008/Kes/205/V/2011
                            <br>Jl. Sama'un Bakri No. 06 RT . 08/01 Lopang Gede
                            <br>Kota Serang - Banten</p>
                        </div>
                        <div class="col-lg-3 pl-0" style="margin-left:176px;">
                          <img src="{{url('/')}}/admin/download.jpg" alt="" height="200px">
                        </div>
                        <div class="col-lg-3 pr-0">
                          <p class="mt-5 mb-2 text-right" style="font-size: 21px">
                            <b>{{$registrasi->pasien->nama_pasien}}</b>
                          </p>
                          <p class="text-right">{{$registrasi->pasien->alamat}}</p>
                        </div>
                      </div>
                      <div class="container-fluid d-flex justify-content-between">
                        <div class="col-lg-3 pl-0">
                          <p class="mb-0 mt-5">Tanggal : {{date('d-m-Y', strtotime($registrasi->waktu_keluar))}}</p>
                          <p >Pemeriksaan : {{$registrasi->perawatan}}</p>
                        </div>
                      </div>
                      <div class="container-fluid mt-5 d-flex justify-content-center w-100">
                        <div class="table-responsive w-100">
                            <h4>Pemeriksaan</h4>
                              <table class="table">
                                <thead>
                                  <tr class="bg-dark text-white">
                                    <th>#</th>
                                    <th>Detail</th>
                                    <th class="text-right">Jumlah</th>
                                    <th class="text-right">Harga</th>
                                    <th class="text-right">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($data_pemeriksaan as $key => $detail)
                                  <tr class="text-right">
                                    <td class="text-left">{{++$key}}</td>
                                    <td class="text-left">{{$detail->detail}}</td>
                                    @if ($registrasi->perawatan != 'Lahiran')
                                    <td>1</td>
                                    @else
                                    @php
                                        $from =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_masuk);
                                        $to =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_keluar);
                                        $diff = $to->diffInDays($from);
                                        if($diff == 0)
                                        {
                                            $diff = 1;
                                        }
                                    @endphp
                                    <td>{{$diff}}
                                    @endif
                                    <td>Rp {{number_format($detail->harga, 2, ',', '.')}}</td>
                                    <td>Rp {{number_format($detail->harga, 2, ',', '.')}}</td>
                                  </tr>
                                  <input type="hidden" name='detail_id[]' value="{{$detail->id}}">
                                  @php
                                    $sum[] = $detail->harga
                                  @endphp
                                  @endforeach
                              </tbody>
                          </table>
                          <table class="table">
                            <thead>
                              <tr class="bg-dark text-white">
                                <th>#</th>
                                <th>Obat</th>
                                <th>Dosis</th>
                                <th class="text-right">Jumlah</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Total</th>
                              </tr>
                            </thead>
                            <hr>
                            <h4>Obat</h4>
                            <tbody>
                                  @foreach(\DB::table('penggunaan_obat')->join('obat', 'obat_id', '=', 'obat.id')->where('pendaftaran_id', $registrasi->id)->get() as $kaa => $ob)
                                    <tr class="text-right">
                                        <td class="text-left">{{++$kaa}}</td>
                                        <td class="text-left">{{$ob->nama}}</td>
                                        <td class="text-left">{{$ob->dosis}}</td>
                                        <td>{{$ob->jumlah}}</td>
                                        <td>Rp {{number_format($ob->harga_jual, 2, ',', '.')}}</td>
                                        <td>{{$ob->jumlah*$ob->harga_jual}}</td>
                                    </tr>
                                    @php
                                      $sum[] = $ob->jumlah*$ob->harga_jual;
                                    @endphp
                                  @endforeach
                                </tbody>
                              </table>
                        </div>
                      </div>
                      <div class="container-fluid mt-5 w-100">
                          <input type="hidden" name="" value="{{array_sum($sum)}}" id="subtotal">
                        <p class="text-right">Sub - Total : <span>Rp {{number_format(array_sum($sum), 2, ',', '.')}}</span></p>
                        <p class="text-right">Diskon
                            <input type="number" required max="100" name="diskon" value="" id="discount" style="margin-left:20px; width:50px; text-align: center">
                            %
                            <input type="hidden" id="tototo" name="tototo">
                            <input type="hidden" name="pendaftaran_id" value="{{$registrasi->id}}">
                            <input type="hidden" name="pasien_id" value="{{$registrasi->pasien_id}}">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        </p>
                        <h4 class="text-right mb-5" id="totalafterdiscount">Total : <span id="total"></span></h4>
                        <hr> </div>
                      <div class="container-fluid w-100">
                        <button href="#" class="btn btn-primary float-right mt-4 ml-2" type="submit">
                          <i class="mdi mdi-printer mr-1"></i>Simpan</button>
                      </div>
                  </form>
                    </div>
                  </div>
                </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection
@section('plugin')
<script type="text/javascript">

$('#discount').keyup(function () {
    var discount = $('#discount').val();
    var total = $('#subtotal').val() * discount / 100;
    console.log(format_number(total, 'Rp'));
    $('#total').html(format_number($('#subtotal').val() - total, 'Rp'));
    $('#tototo').val($('#subtotal').val() - total);
});
</script>
@endsection
