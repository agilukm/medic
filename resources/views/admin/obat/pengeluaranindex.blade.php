@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pengeluaran Obat</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Pemeriksaan</th>
                  <th>Pasien</th>
                  <th>Jumlah</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->obat->nama}}</td>
                  <td>{{$da->pendaftaran->perawatan}}</td>
                  <td>{{$da->pendaftaran->pasien->nama_pasien}}</td>
                  <td>{{$da->jumlah}}</td>
                  <td>{{$da->created_at->format('d-m-Y')}}</td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('plugin')
@endsection
