@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Obat</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Satuan</th>
                  <th>Harga Jual</th>
                  <th>Stok</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->nama}}</td>
                  <td>{{$da->satuan}}</td>
                  <td>Rp {{number_format($da->harga_jual, 2, ',', '.')}}</td>
                  <td>{{$da->stok}}</td>
                  <td>
                      <a href="{{ url('/obat/edit/'.$da->id)}}" class="btn btn-info">Edit</a> <br>
                      <a href="{{ url('/obat/hapus/'.$da->id)}}" class="btn btn-danger">Hapus</a> <br>
                  </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="modalTambah">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="forms-sample" method="post" action="{{url('/obat/tambah')}}">
                      <div class="form-group">
                        <label for="exampleInputName1">Nama Obat</label>
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="text" name="nama" placeholder="Nama Obat" class="form-control">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail3">Satuan</label>
                        <input required type="text" class="form-control" name="satuan" placeholder="Satuan">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Harga Jual</label>
                        <input required type="text" class="form-control" name="harga_jual" placeholder="Harga Jual">
                      </div>

                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Reset</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

@endsection
@section('plugin')
@endsection
