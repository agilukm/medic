@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pemasukan Obat</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Jumlah</th>
                  <th>Total</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->created_at->format('d-m-Y')}}</td>
                  <td>{{$da->jumlah}}</td>
                  <td>Rp {{number_format($da->total, 2, ',', '.')}}</td>
                  <td>
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#detailModal" onclick="detail({{$da->id}})">Detail</a> <br>

                  </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="modalTambah">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button type="button" name="button" class="btn btn-primary" id="plus">+</button>
                    <form class="forms-sample" method="post" action="{{url('/obat/pemasukan/tambah')}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Obat</td>
                                    <td>Jumlah</td>
                                    <td>Total</td>
                                </tr>
                            </thead>
                            <tbody id="formm">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                            </tbody>
                        </table>


                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Reset</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->
            <div class="modal fade" id="detailModal">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width:1080px; right:50%;">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <h1>Detail Pemasukan Obat</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>No</th>
                                    <th>Obat</th>
                                    <th>Jumlah</th>
                                    <th>Total</th>
                                  </tr>
                                </thead>
                                <tbody id="isidetail">

                                </tbody>

                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
@endsection
@section('plugin')
<script type="text/javascript">
    var hash = 1;
    $('#plus').click(function() {
        var input = '<tr id="'+hash+'"> <td> <select required class="form-control" name="obat_id[]"> <option value="">Pilih Obat</option>  @foreach($obat as $key => $da) <option value="{{$da->id}}">{{$da->nama}}</option> @endforeach </select>  </td> <td> <input required type="number" name="jumlah[]" placeholder="Jumlah Obat" class="form-control"> <td> <input type="number" required name="total[]" placeholder="Total Harga" class="form-control"> </td> </tr>';
        $('#formm').append(input);
    });

    function detail(id, nama_bayi) {
        $.ajax({
                url: "{{ url('obat/pemasukan/detail/') }}/" + id,
                type: "GET",
                dataType: "html",
                success: function (result) {
                    $("#isidetail").html('');
                    var datas = JSON.parse(result);
                    var i = 1;
                    $.each(datas, function(k, data) {
                        $('#isidetail').append(
                            $("<tr>"),
                            $("<td>").text(i),
                            $("<td>").text(data.nama),
                            $("<td>").text(data.jumlah),
                            $("<td>").text(data.total),
                            $("</tr>")
                        );
                        i++;
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal!", "Silahkan coba lagi", "error");
                }
            });
    };
</script>
@endsection
