@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pengunaan Pemeriksaan Obat</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Pemeriksaan</th>
                  <th>Obat</th>
                  <th>Jumlah</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->jenis}}</td>
                  <td>{{$da->obat->nama}}</td>
                  <td>{{$da->jumlah}}</td>
                  <td>
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#detailModal" onclick="detail({{$da->id}})">Edit</a> <br>
                      <a href="{{ url('/obat/penggunaan/hapus/'.$da->id)}}" class="btn btn-info">Hapus</a> <br>
                  </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="modalTambah">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button type="button" name="button" class="btn btn-primary" id="plus">+</button>
                    <form class="forms-sample" method="post" action="{{url('/obat/penggunaan/tambah')}}">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Jenis Perawatan</td>
                                    <td>Nama Obat</td>
                                    <td>Jumlah</td>
                                </tr>
                            </thead>
                            <tbody id="formm">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                            </tbody>
                        </table>


                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Reset</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->
            <div class="modal fade" id="detailModal">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width:1080px; right:50%;">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <h1>Edit Penggunaan Perawatan Obat</h1>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <form class="forms-sample" method="post" action="{{url('/obat/penggunaan/edit')}}">
                                  <div class="form-group">
                                    <label for="exampleInputName1">Perawatan</label>
                                    <input type="text" name="" class="form-control" placeholder="Perawatan" readonly value="" id="perawatan">
                                    <input type="hidden" name="penggunaan_id" class="form-control" placeholder="Nama Bayi" readonly value="" id="penggunaan_id">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputName1">Obat</label>
                                    <input type="text" name="" class="form-control" placeholder="Obat" readonly value="" id="obat">
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputName1">Jumlah</label>
                                    <input type="text" name="jumlah" class="form-control" placeholder="Jumlah" value="" id="jumlah">
                                  </div>
                                  <button type="submit" class="btn btn-success mr-2">Submit</button>
                                  <button class="btn btn-light" type="reset">Reset</button>
                                </form>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
@endsection
@section('plugin')
<script type="text/javascript">
    var hash = 1;
    $('#plus').click(function() {
        var input = '<tr id="'+hash+'"><td><select required class="form-control" name="jenis[]"> <option value="">Pilih Jenis Perawatan</option>  <option value="Imunisasi">Imunisasi</option> <option value="Lahiran">Lahiran</option> <option value="Kandungan">Kandungan</option> <option value="Kb">Kb</option></select>  </td> <td> <select required class="form-control" name="obat_id[]"> <option value="">Pilih Obat</option>  @foreach($obat as $key => $da) <option value="{{$da->id}}">{{$da->nama}}</option> @endforeach </select>  </td> <td> <input required type="number" name="jumlah[]" placeholder="Jumlah Obat" class="form-control"> </tr>';
        $('#formm').append(input);
    });

    function detail(id) {
        $.ajax({
                url: "{{ url('obat/penggunaan/detail/') }}/" + id,
                type: "GET",
                dataType: "html",
                success: function (result) {
                    $("#isidetail").html('');
                    var datas = JSON.parse(result);
                    var i = 1;
                    console.log(result);
                    $('#penggunaan_id').val(datas.id);
                    $('#perawatan').val(datas.jenis);
                    $('#obat').val(datas.nama);
                    $('#jumlah').val(datas.jumlah);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal!", "Silahkan coba lagi", "error");
                }
            });
    };
</script>
@endsection
