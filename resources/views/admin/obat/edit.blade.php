@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Obat</h4>
          
          <form class="forms-sample" method="post" action="{{url('/obat/simpan/'.$data->id)}}">
            <div class="form-group">
              <label for="exampleInputName1">Jenis Perawatan</label>
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              <input type="text" name="nama" placeholder="Nama Obat" class="form-control" value="{{$data->nama}}">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail3">Satuan</label>
              <input required type="text" class="form-control" name="satuan" placeholder="Satuan" value="{{$data->satuan}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Harga Jual</label>
              <input required type="text" class="form-control" name="harga_jual" placeholder="Harga Jual" value="{{$data->harga_jual}}">
            </div>

            <button type="submit" class="btn btn-success mr-2">Submit</button>
            <button class="btn btn-light" type="reset">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection
@section('plugin')
@endsection
