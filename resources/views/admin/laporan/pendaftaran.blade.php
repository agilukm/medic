<!DOCTYPE html>
<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <title></title>
   </head>
   <body>
       <style media="screen">
         td, th {
           border: 1px solid black;
           }
           .alignright {
             padding-right: 100px;
           }
           #table {
               border-collapse: collapse;
           }
         </style>
         <center>
         <b>LAPORAN DATA PENDAFTARAN</b>
         <br>
         BIDAN PRAKTEK MANDIRI
         <br>
         PERIODE {{date('d-m-Y', strtotime($awal))}} s/d {{date('d-m-Y', strtotime($akhir))}}
     </center>
     </br>
     <br>
     <br>
        <table id="table" style="width:100%">
         <thead>
            <tr>
               <th align="center">No</th>
               <th align="center">Kode Registrasi</th>
               <th align="center">Nama Pasien</th>
               <th align="center">Jenis Pemeriksaan</th>
               <th align="center">Tanggal Masuk</th>
            </tr>
         </thead>
         <tbody>
            @foreach($pendaftarans as $key => $pendaftaran)
            <tr>
               <td align="center">{{++$key}}</td>
               <td align="center">{{$pendaftaran->kode_registrasi}}</td>
               <td align="center">{{$pendaftaran->pasien->nama_pasien}}</td>
               <td align="center">{{$pendaftaran->perawatan}}</td>
               <td align="center">{{date('d-m-Y H:i:s', strtotime($pendaftaran->waktu_daftar))}}</td>
            </tr>
            @endforeach
         </tbody>
      </table>
      <div style="float:right; margin-right:50px">
    <br>
    <br>
    <br>
          Serang, {{date('d')}} <?php
          // FUNGSI BULAN DALAM BAHASA INDONESIA
          function bulan($bln){
          $bulan = $bln;
          Switch ($bulan){
           case 1 : $bulan="Januari";
           Break;
           case 2 : $bulan="Februari";
           Break;
           case 3 : $bulan="Maret";
           Break;
           case 4 : $bulan="April";
           Break;
           case 5 : $bulan="Mei";
           Break;
           case 6 : $bulan="Juni";
           Break;
           case 7 : $bulan="Juli";
           Break;
           case 8 : $bulan="Agustus";
           Break;
           case 9 : $bulan="September";
           Break;
           case 10 : $bulan="Oktober";
           Break;
           case 11 : $bulan="November";
           Break;
           case 12 : $bulan="Desember";
           Break;
           }
          return $bulan;
          }

          //CARA MEMANGGIL FUNGSI BULAN

          $bulan = bulan(date("m"));
          echo $bulan;

          //CARA MEMANGGIL FUNGSI BULAN

          ?> {{date('Y')}}
          <br>
          Pembuat Laporan (BPM)
          <br>
          <br>
          <br>
          <br>
          <br>
          Kusniah, S.ST
          <br>
          NIP. 196905167 199203 2 008
   </body>
</html>
