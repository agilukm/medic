<!DOCTYPE html>
<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <title></title>
   </head>
   <body>
      <style media="screen">
      </style>
      <style media="screen">
      td, th {
        border: 1px solid black;
        }
        .alignright {
          padding-right: 100px;
        }
        #table {
            border-collapse: collapse;
        }
      </style>
      <center>
      <b>LAPORAN PERSALINAN</b>
      <br>
      BIDAN PRAKTEK MANDIRI
      <br>
      PERIODE {{date('d-m-Y', strtotime($awal))}} s/d {{date('d-m-Y', strtotime($akhir))}}
  </center>
  </br>
  <br>
  <br>
      <table id="table" id="table" style="width:100%;">
          <thead>
              <tr>
                <th align="center">No</th>
                <th align="center">Tanggal</th>
                <th align="center">Nama Ibu/Suami</th>
                <th align="center">Umur</th>
                <th align="center">Alamat</th>
                <th align="center">Jam Lahir</th>
                <th align="center">JK</th>
                <th align="center">BB</th>
                <th align="center">PB</th>
                <th align="center">Therapi</th>
                <th align="center">Lama Perawatan</th>

              </tr>
            </thead>
            <tbody>
                @foreach($data as $key => $d)
              <tr>
                <td  align="center">{{++$key}}</td>
                <td  align="center">{{$d->created_at->format('d-m-Y')}}</td>
                <td  align="center">{{$d->pasien->nama_pasien}}</td>
                @php
                  $to = \Carbon\Carbon::now();
                  $from = \Carbon\Carbon::createFromFormat('Y-m-d', $d->pasien->tgl_lahir);
                 @endphp
                <td  align="center">{{$to->diffInYears($from)}}</td>
                <td  align="center">{{$d->pasien->alamat}}</td>
                <td  align="center">{{date('d-m-Y H:i:s', strtotime($d->jam_lahir))}}</td>
                <td  align="center">{{$d->jenis_kelamin}}</td>
                <td  align="center">{{$d->berat_badan}}</td>
                <td  align="center">{{$d->pb}}</td>
                <td  align="center">{{$d->therapi}}</td>
                @php
                $diff = '-';
                $registrasi = \DB::table('log_pemeriksaan')->join('registrasi', 'registrasi.id', '=', 'log_pemeriksaan.pendaftaran_id')->where('jenis', 'Lahiran')->where('jenis_id', $d->id)->first();
                if($registrasi){
                $from =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_masuk);
                $to =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_keluar);
                $diff = $to->diffInDays($from);
                if($diff == 0)
                {
                    $diff = 1;
                }
                }
                @endphp
                <td  align="center">{{$diff}} hari</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <div style="float:right; margin-right:50px">
        <br>
        <br>
        <br>
              Serang, {{date('d')}} <?php
              // FUNGSI BULAN DALAM BAHASA INDONESIA
              function bulan($bln){
              $bulan = $bln;
              Switch ($bulan){
               case 1 : $bulan="Januari";
               Break;
               case 2 : $bulan="Februari";
               Break;
               case 3 : $bulan="Maret";
               Break;
               case 4 : $bulan="April";
               Break;
               case 5 : $bulan="Mei";
               Break;
               case 6 : $bulan="Juni";
               Break;
               case 7 : $bulan="Juli";
               Break;
               case 8 : $bulan="Agustus";
               Break;
               case 9 : $bulan="September";
               Break;
               case 10 : $bulan="Oktober";
               Break;
               case 11 : $bulan="November";
               Break;
               case 12 : $bulan="Desember";
               Break;
               }
              return $bulan;
              }

              //CARA MEMANGGIL FUNGSI BULAN

              $bulan = bulan(date("m"));
              echo $bulan;

              //CARA MEMANGGIL FUNGSI BULAN

              ?> {{date('Y')}}
              <br>
              Pembuat Laporan (BPM)
              <br>
              <br>
              <br>
              <br>
              <br>
              Kusniah, S.ST
              <br>
              NIP. 196905167 199203 2 008
   </body>
</html>
