<!DOCTYPE html>
<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <title></title>
      <style media="screen">
      td, th {
        border: 1px solid black;
        }
        .alignright {
          padding-right: 100px;
        }
        #table {
            border-collapse: collapse;
        }
      </style>
   </head>
   <body>
      <style media="screen">
      </style>
      <center>
      <b>LAPORAN DATA IMUNISASI</b>
      <br>
      BIDAN PRAKTEK MANDIRI
      <br>
      PERIODE {{date('d-m-Y', strtotime($awal))}} s/d {{date('d-m-Y', strtotime($akhir))}}
  </center>
  </br>
  <br>
  <br>
  <center>
      <table id="table" style="width:100%">
          <thead>
            <tr>
              <th rowspan="2" align="center">No</th>
              <th rowspan="2" align="center">Nama Orang Tua</th>
              <th rowspan="2" align="center">Nama Bayi</th>
              <th colspan="2" align="center">Umur</th>
              <th  align="center">BB</th>
              <th colspan="7" align="center">Imunisasi yang diberikan</th>
              <th rowspan="2" align="center">Tanggal</th>
            </tr>
            <tr>
                <th align="center">L</th>
                <th align="center">P</th>
                <th align="center"></th>
                <th align="center">BCG</th>
                <th align="center">DPT <br> PENTA <br> BIO I <br>  </th>
                <th align="center">DPT <br> PENTA <br> BIO II <br>  </th>
                <th align="center">DPT <br> PENTA <br> BIO III <br>  </th>
                <th align="center">CAMPAK/RUBELA <br>  </th>
                <th align="center">POLIO  </th>
                <th align="center">KETERANGAN </th>
            </tr>
          </thead>
          <tbody>
          @foreach($data as $key => $data)
            <tr>
              <td align="center">{{++$key}}</td>
              <td align="center">{{$data->pasien->nama_pasien}} / {{$data->pasien->nama_suami}}</td>
              <td align="center">{{$data->nama_bayi}}</td>
              <td align="center">{{$data->umur}}</td>
              <td align="center">{{$data->jenis_kelamin}}</td>
              <td align="center">{{$data->berat_badan}}</td>
              <td align="center">@if($data->jenis_imunisasi == 'BCG') {{$data->jenis_imunisasi}} @endif</td>
              <td align="center">@if($data->jenis_imunisasi == 'DPT PENTA BIO 1') DPT PENTA BIO I @endif</td>
              <td align="center">@if($data->jenis_imunisasi == 'DPT PENTA BIO 2') DPT PENTA BIO II @endif</td>
              <td align="center">@if($data->jenis_imunisasi == 'DPT PENTA BIO 3') DPT PENTA BIO III @endif</td>
              <td align="center">@if($data->jenis_imunisasi == 'CAMPAK / RUBELLA') CAMPAK / RUBELLA @endif</td>
              <td align="center">@if($data->jenis_imunisasi == 'Polio') {{$data->jenis_imunisasi}} @endif</td>
              <td align="center"></td>
              <td align="center">{{$data->created_at->format('d-m-Y')}}</td>
            </tr>
            @endforeach
          </tbody>
      </table>
  </center>



  <div style="float:right; margin-right:50px">
<br>
<br>
<br>
      Serang, {{date('d')}} <?php
      // FUNGSI BULAN DALAM BAHASA INDONESIA
      function bulan($bln){
      $bulan = $bln;
      Switch ($bulan){
       case 1 : $bulan="Januari";
       Break;
       case 2 : $bulan="Februari";
       Break;
       case 3 : $bulan="Maret";
       Break;
       case 4 : $bulan="April";
       Break;
       case 5 : $bulan="Mei";
       Break;
       case 6 : $bulan="Juni";
       Break;
       case 7 : $bulan="Juli";
       Break;
       case 8 : $bulan="Agustus";
       Break;
       case 9 : $bulan="September";
       Break;
       case 10 : $bulan="Oktober";
       Break;
       case 11 : $bulan="November";
       Break;
       case 12 : $bulan="Desember";
       Break;
       }
      return $bulan;
      }

      //CARA MEMANGGIL FUNGSI BULAN

      $bulan = bulan(date("m"));
      echo $bulan;

      //CARA MEMANGGIL FUNGSI BULAN

      ?> {{date('Y')}}
      <br>
      Pembuat Laporan (BPM)
      <br>
      <br>
      <br>
      <br>
      <br>
      Kusniah, S.ST
      <br>
      NIP. 196905167 199203 2 008
  </div>
   </body>
</html>
