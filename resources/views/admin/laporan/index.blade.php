@extends('admin.layout')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Laporan</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Laporan</li>
                <li class="breadcrumb-item active">Cetak Laporan</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-title">
                                <center><h4 align="center">Pendaftaran</h4></center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('laporan/pendaftaran')}}" method="get" >
                                    <div class="form-group">
                                        <br>
                                        <p>Tanggal Mulai</p></center>
                                        <input type="date" required name="mulai" value="" class="form-control">
                                        <br>
                                        <br><p>Tanggal Akhir</p></center>
                                        <input type="date" required name="akhir" value="" class="form-control">
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-title">
                                <center><h4 align="center">Pemeriksaan</h4></center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('laporan/perawatan')}}" method="get">
                                    <div class="form-group">
                                        <br>
                                        <p>Tipe Pemeriksaan</p></center>
                                        <select class="form-control" required name="jenis">
                                            <option value="">Pilih</option>
                                            <option value="Kb">Kb</option>
                                            <option value="Imunisasi">Imunisasi</option>
                                            <option value="Lahiran">Lahiran</option>
                                            <option value="Kandungan">Kandungan</option>
                                        </select>
                                        <p>Tanggal Mulai</p></center>
                                        <input type="date" name="mulai" value="" class="form-control">
                                        <p>Tanggal Akhir</p></center>
                                        <input type="date" name="akhir" value="" class="form-control">
                                    </div>
                                    <br>
                                    <br>
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-title">
                                <center><h4 align="center">Pemeriksaan Pasien</h4></center>
                            </div>
                            <div class="card-body">
                                <form class="" action="{{url('laporan/perawatan/pasien')}}" method="get" ><br>
                                <p>Pilih Pasien</p></center>

                                    <select class="form-control" required name="pasien_id">
                                        <option value="">Pilih</option>
                                        @foreach(\DB::table('pasien')->get() as $key => $data)
                                            <option value="{{$data->id}}">{{$data->nama_pasien}} / {{$data->ktp}}</option>
                                         @endforeach
                                    </select>
                                    <p>Tipe Pemeriksaan</p>
                                    <select class="form-control" required name="jenis">
                                        <option value="">Pilih</option>
                                        <option value="Kb">Kb</option>
                                        <option value="Imunisasi">Imunisasi</option>
                                        <option value="Lahiran">Lahiran</option>
                                        <option value="Kandungan">Kandungan</option>
                                    </select>
                                    <p>Tanggal Mulai</p></center>
                                    <input type="date" name="mulai" value="" class="form-control">
                                    <p>Tanggal Akhir</p></center>
                                    <input type="date" name="akhir" value="" class="form-control">
                                    <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                </form>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-title">
                                    <center><h4 align="center">Pembayaran</h4></center>
                                </div>
                                <div class="card-body">
                                    <form class="" action="{{url('laporan/pembayaran')}}" method="get" >
                                        <div class="form-group">
                                            <br>
                                            <p>Tanggal Mulai</p></center>
                                            <input type="date" name="mulai" value="" class="form-control">
                                            <br>
                                            <br><p>Tanggal Akhir</p></center>
                                            <input type="date" name="akhir" value="" class="form-control">
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                    </form>
                                </div>
                            </div>
                        </div>

                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-title">
                                        <center><h4 align="center">Pasien</h4></center>
                                    </div>
                                    <div class="card-body">
                                        <form class="" action="{{url('laporan/pasien')}}" method="get" >
                                            <div class="form-group">
                                                <br>
                                                <!-- <p>Tanggal Mulai</p></center> -->
                                                <!-- <input type="date" name="" value="" class="form-control"> -->
                                                <br>
                                                <center>  <p>Cetak Data Pasien</p></center>
                                                <!-- <br><p>Tanggal Akhir</p></center> -->
                                                <!-- <input type="date" name="" value="" class="form-control"> -->
                                                <br>
                                                <br>
                                                <br>
                                            </div>
                                            <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-title">
                                        <center><h4 align="center">Obat</h4></center>
                                    </div>
                                    <div class="card-body">
                                        <form class="" action="{{url('laporan/obat')}}" method="get" >
                                            <div class="form-group">
                                                <br>
                                                <!-- <p>Tanggal Mulai</p></center> -->
                                                <!-- <input type="date" name="" value="" class="form-control"> -->
                                                <br>
                                                <center>  <p>Cetak Data Obat</p></center>
                                                <!-- <br><p>Tanggal Akhir</p></center> -->
                                                <!-- <input type="date" name="" value="" class="form-control"> -->
                                                <br>
                                                <br>
                                                <br>
                                            </div>
                                            <center><button type="submit" class="btn btn-success" name="button">Cetak</button></center>
                                        </form>
                                    </div>
                                </div>
                            </div>

                </div>
        </div>
        </div>
        </div>
        </div>
        </div>


        @endsection
        @section('plugin')
        @endsection
