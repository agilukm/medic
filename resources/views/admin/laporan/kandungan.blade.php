<!DOCTYPE html>
<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <title></title>
   </head>
   <body>
       <style media="screen">
       td, th {
         border: 1px solid black;
         }
         .alignright {
           padding-right: 100px;
         }
         #table {
             border-collapse: collapse;
         }
       </style>
       <center>
       <b>LAPORAN ANTENATAL CARE</b>
       <br>
       BIDAN PRAKTEK MANDIRI
       <br>
       PERIODE {{date('d-m-Y', strtotime($awal))}} s/d {{date('d-m-Y', strtotime($akhir))}}
   </center>
   </br>
   <br>
   <br>
      <table id="table" style="width:100%;">
          <thead>
              <tr>
                <th align="center">No</th>
                <th align="center">Nama</th>
                <th align="center">Suami</th>
                <th align="center">Umur</th>
                <th align="center">Anamnesis</th>
                <th align="center">GPA</th>
                <th align="center">BB</th>
                <th align="center">TD</th>
                <th align="center">TFU</th>
                <th align="center">LLA</th>
                <th align="center">DJJ <br> (x/mnt)</th>
                <th align="center">JMLH <br> JANIN</th>
                <th align="center">PRESEN<br>TASI</th>
                <th align="center">Status <br> Imunisasi</th>
                <th align="center">Tablet FE</th>
                <th align="center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $key => $da)
              <tr>
                <td align="center">{{++$key}}</td>
                <td align="center">{{$da->pasien->nama_pasien}}</td>
                <td align="center">{{$da->pasien->nama_suami}}</td>
                <td align="center">
                    <?php
                    $d1 = new DateTime();
$d2 = new DateTime($da->pasien->tgl_lahir);

$diff = $d2->diff($d1);

echo $diff->y;
?>
                </td>
                <td align="center">{{$da->anamnesis}}</td>
                <td align="center">{{$da->gpa}}</td>
                <td align="center">{{$da->berat_badan}}</td>
                <td align="center">{{$da->tekanan_darah}}</td>
                <td align="center">{{$da->tinggi_fundus_uteri}}</td>
                <td align="center">{{$da->lingkar_lengan_atas}}</td>
                <td align="center">{{$da->denyut_jantung_janin}}</td>
                <td align="center">{{$da->jumlah_janin}}</td>
                <td align="center">{{$da->presentasi}}</td>
                <td align="center">{{$da->status_imunisasi}}</td>
                <td align="center">{{$da->tablet_fe}}</td>
                <td align="center">{{$da->created_at->format('d-m-Y')}}</td>
              </tr>
              @endforeach
            </tbody>
      </table>


        <div style="float:right; margin-right:50px">
      <br>
      <br>
      <br>
            Serang, {{date('d')}} <?php
            // FUNGSI BULAN DALAM BAHASA INDONESIA
            function bulan($bln){
            $bulan = $bln;
            Switch ($bulan){
             case 1 : $bulan="Januari";
             Break;
             case 2 : $bulan="Februari";
             Break;
             case 3 : $bulan="Maret";
             Break;
             case 4 : $bulan="April";
             Break;
             case 5 : $bulan="Mei";
             Break;
             case 6 : $bulan="Juni";
             Break;
             case 7 : $bulan="Juli";
             Break;
             case 8 : $bulan="Agustus";
             Break;
             case 9 : $bulan="September";
             Break;
             case 10 : $bulan="Oktober";
             Break;
             case 11 : $bulan="November";
             Break;
             case 12 : $bulan="Desember";
             Break;
             }
            return $bulan;
            }

            //CARA MEMANGGIL FUNGSI BULAN

            $bulan = bulan(date("m"));
            echo $bulan;

            //CARA MEMANGGIL FUNGSI BULAN

            ?> {{date('Y')}}
            <br>
            Pembuat Laporan (BPM)
            <br>
            <br>
            <br>
            <br>
            <br>
            Kusniah, S.ST
            <br>
            NIP. 196905167 199203 2 008
   </body>
</html>
