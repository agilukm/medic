@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Lahiran {{$pasien->nama_pasien}}</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
              <table class="table" id="myTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Orang Tua</th>
                    <th>Umur</th>
                    <th>GPA</th>
                    <th>Jam Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Berat Badan</th>
                    <th>PB</th>
                    <th>Therapi</th>
                    <th>Lama Perawatan</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($detail_pasien as $key => $d)
                  <tr>
                    <td>{{++$key}}</td>
                    <td>{{$d->pasien->nama_pasien}}</td>
                    @php
                      $to = \Carbon\Carbon::now();
                      $from = \Carbon\Carbon::createFromFormat('Y-m-d', $d->pasien->tgl_lahir);
                     @endphp
                    <td>{{$to->diffInYears($from)}}</td>
                    <td>{{$d->gpa}}</td>
                    <td>{{date('d-m-Y H:i:s', strtotime($d->jam_lahir))}}</td>
                    <td>{{$d->jenis_kelamin}}</td>
                    <td>{{$d->berat_badan}}</td>
                    <td>{{$d->pb}}</td>
                    <td>{{$d->therapi}}</td>
                    @php
                    $diff = '-';
                    $registrasi = \DB::table('log_pemeriksaan')->join('registrasi', 'registrasi.id', '=', 'log_pemeriksaan.pendaftaran_id')->where('jenis', 'Lahiran')->where('jenis_id', $d->id)->first();

                    if($registrasi){
                        $from =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_masuk);
                    $to =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_keluar);
                    $diff = $to->diffInDays($from);
                    if($diff == 0)
                    {
                        $diff = 1;
                    }
                    }
                    @endphp
                    <td>{{$diff}} hari</td>
                    <td>{{$d->created_at->format('Y-m-d')}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:1080px; right:50%;">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <h1>Detail Imunisasi</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Umur</th>
                            <th>Jenis Kelamin</th>
                            <th>Berat Badan</th>
                            <!-- <th>Imunisasi yang diberikan</th>
                            <th>Tanggal</th> -->
                          </tr>
                        </thead>
                        <tbody id="isidetail">

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <h2>Tambah Data Lahiran</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <span>Data anak yang baru lahir</span>
                            <hr>
                            <form class="forms-sample" method="post" action="{{url('/perawatan/Lahiran/tambah')}}">
                              <div class="form-group">
                                <label for="exampleInputName1">Berat Badan</label>
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <input type="number" step="0.01" min="1" name="berat_badan" class="form-control" placeholder="Berat Badan">
                                <input type="hidden" name="pasien_id" class="form-control" value="{{$pasien->id}}">
                                <input type="hidden" name="pendaftaran_id" class="form-control" value="{{$pendaftaran_id}}">

                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Jam Lahir</label>
                                <input required type="datetime-local" class="form-control" name="jam_lahir" id="kodepasien" placeholder="Jam Lahir"  min="2000-01-01" value="<?php echo date('Y-m-d\TH:i')?>" max="<?php echo date('Y-m-d\TH:i')?>">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Jenis Kelamin</label>
                                <select required type="text" name="jenis_kelamin"  class="form-control">
                                    <option value="">Pilih</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">PB</label>
                                <input required type="text" name="pb" class="form-control" id="tgllahir" placeholder="PB">
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">Therapi</label>
                                <input required type="text" name="Therapi" class="form-control" id="tgllahir" placeholder="Therapi">
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">Obat</label>
                                <select class="form-control select2 " name="obat_id[]" multiple style="width:100%">
                                    <option value="">Pilih</option>
                                    @foreach(\DB::table('obat')->leftjoin('obat_pemeriksaan','obat.id', '=', 'obat_id')->where('stok', '>=', 1)->where('jenis', 'Lahiran')->get() as $key => $ob)
                                        <option value="{{$ob->obat_id}}">{{$ob->nama}}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Dosis</label>
                                <div class="dosis">

                                </div>
                              </div>
                              <button type="submit" class="btn btn-success mr-2">Submit</button>
                              <button class="btn btn-light" type="reset">Reset</button>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
<script type="text/javascript">
$(".select2").on("select2:select", function (e) {
        var lastSelectedId = e.params.data.id;
        var lastSelectedName = e.params.data.text;
        $.ajax({
            url: "{{ url('obat/haha/') }}/" + lastSelectedId,
            type: "GET",
            dataType: "html",
            success: function (result) {
                var datas = JSON.parse(result);
                var duplicate = '<div class="row" id="obat'+lastSelectedId+'"> <div class="col-md-6"> <div class="form-group">  <input type="text" name="" value="'+datas.nama+'" readonly class="form-control"> </div> </div> <div class="col-md-6"> <div class="form-group"> <input type="text" required name="dosis['+lastSelectedId+']" value="" class="form-control" placeholder="Dosis"> </div> </div> </div>';
                $(".dosis").append(duplicate);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
    });

    $(".select2").on("select2:unselect", function (e) {
                var lastSelectedId = e.params.data.id;
                $('#obat'+lastSelectedId).remove();
            });
</script>
@endsection
