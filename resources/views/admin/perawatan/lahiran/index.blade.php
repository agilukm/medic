@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Lahiran</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Orang Tua</th>
                  <th>Umur</th>
                  <th>Jam Lahir</th>
                  <th>Jenis Kelamin</th>
                  <th>Berat Badan</th>
                  <th>PB</th>
                  <th>Therapi</th>
                  <th>Lama Perawatan</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($data as $key => $d)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$d->pasien->nama_pasien}}</td>
                  @php
                    $to = \Carbon\Carbon::now();
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d', $d->pasien->tgl_lahir);
                   @endphp
                  <td>{{$to->diffInYears($from)}}</td>
                  <td>{{date('d-m-Y H:i:s', strtotime($d->jam_lahir))}}</td>
                  <td>{{$d->jenis_kelamin}}</td>
                  <td>{{$d->berat_badan}}</td>
                  <td>{{$d->pb}}</td>
                  <td>{{$d->therapi}}</td>
                  @php
                  $diff = '-';
                  $registrasi = \DB::table('log_pemeriksaan')->join('registrasi', 'registrasi.id', '=', 'log_pemeriksaan.pendaftaran_id')->where('jenis', 'Lahiran')->where('jenis_id', $d->id)->first();
                  if($registrasi){
                      $from =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_masuk);
                  $to =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_keluar);
                  $diff = $to->diffInDays($from);
                  if($diff == 0)
                  {
                      $diff = 1;
                  }
                  }
                  @endphp
                  <td>{{$diff}} hari</td>
                  <td>{{$d->created_at->format('d-m-Y')}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                            <th>No</th>
                            <th>Nama Jasa</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama Jasa</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
@endsection
