@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data KB {{$pasien->nama_pasien}}</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Berat Badan</th>
                  <th>Tekanan Darah</th>
                  <th>Tanggal</th>
                  <th>Tanggal Kembali</th>
                  <!-- <th>Aksi</th> -->
                </tr>
              </thead>
              <tbody>
                  @foreach($detail_pasien as $key => $detail)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$detail->berat_badan}}</td>
                  <td>{{$detail->tekanan_darah}}</td>
                  <td>{{$detail->created_at->format('Y-m-d')}}</td>
                  <td>{{$detail->tanggal_kembali}}</td>

                  @endforeach
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:1080px; right:50%;">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <h1>Detail Imunisasi</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Umur</th>
                            <th>Jenis Kelamin</th>
                            <th>Berat Badan</th>
                            <!-- <th>Imunisasi yang diberikan</th>
                            <th>Tanggal</th> -->
                          </tr>
                        </thead>
                        <tbody id="isidetail">

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <h2>Tambah Data</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="forms-sample" method="post" action="{{url('/perawatan/Kb/tambah')}}">
                              <div class="form-group">
                                <label for="exampleInputName1">Berat Badan</label>
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <input type="number" step="0.01" name="berat_badan" class="form-control" placeholder="Berat Badan">
                                <input type="hidden" name="pasien_id" class="form-control" value="{{$pasien->id}}">
                                <input type="hidden" name="pendaftaran_id" class="form-control" value="{{$pendaftaran_id}}">

                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Tekanan Darah</label>
                                <input required type="text" class="form-control" name="tekanan_darah" id="kodepasien" placeholder="Tekanan Darah">
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">Tanggal Kembali</label>
                                <select class="form-control"  required name="tipe" class="form-control" id="tipe">
                                    <option value="">Pilih</option>
                                    <option value="1 Bulan">1 Bulan</option>
                                    <option value="3 Bulan">3 Bulan</option>
                                </select>
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">Obat</label>
                                <div class="selee">

                                </div>

                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Dosis</label>
                                <div class="dosis">

                                </div>
                              </div>
                              <button type="submit" class="btn btn-success mr-2">Submit</button>
                              <button class="btn btn-light" type="reset">Reset</button>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
<script type="text/javascript">

$("#tipe").change( function() {
    if($("#tipe option:selected").text() == '1 Bulan')
    {
        $('.selee').html('');
        $('.dosis').html('');
        $('.selee').html('<select class="form-control selectz2 " name="obat_id[]" multiple style="width:100%"><option value="">Pilih</option>@foreach(\DB::table('obat')->join('obat_pemeriksaan', 'obat.id', '=', 'obat_id')->where('obat.nama', '=', 'Cyclofem')->where('jenis', 'Kb')->whereRaw('stok >= jumlah')->get() as $key => $ob)<option value="{{$ob->obat_id}}">{{$ob->nama}}</option>@endforeach</select>');
        console.log('babi');
        $('.selectz2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });
        $(".selectz2").on("select2:select", function (e) {
            $(".dosis").html("");
                var lastSelectedId = e.params.data.id;
                var lastSelectedName = e.params.data.text;
                $("#obat"+lastSelectedId).remove();
                $.ajax({
                    url: "{{ url('obat/haha/') }}/" + lastSelectedId,
                    type: "GET",
                    dataType: "html",
                    success: function (result) {
                        var datas = JSON.parse(result);
                        var duplicate = '<div class="row" id="obat'+lastSelectedId+'"> <div class="col-md-6"> <div class="form-group">  <input type="text" name="" value="'+datas.nama+'" readonly class="form-control"> </div> </div> <div class="col-md-6"> <div class="form-group"> <input type="text" name="dosis['+lastSelectedId+']" value="" class="form-control" placeholder="Dosis"> </div> </div> </div>';
                        $('.dosis').html('');
                        $(".dosis").html(duplicate);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal!", "Silahkan coba lagi", "error");
                    }
                });
            });

            $(".selectz2").on("select2:unselect", function (e) {
                        var lastSelectedId = e.params.data.id;
                        $('#obat'+lastSelectedId).remove();
                    });
    }
    else if($("#tipe option:selected").text() == '3 Bulan')
    {
        $('.selee').html('');
        $('.dosis').html('');
        $('.selee').html('<select class="form-control selectz2 " name="obat_id[]"  style="width:100%"><option value="">Pilih</option>@foreach(\DB::table('obat')->join('obat_pemeriksaan', 'obat.id', '=', 'obat_id')->whereIn('obat.nama', ['Depo 1cc', 'Depo 3cc'])->where('jenis', 'Kb')->whereRaw('stok >= jumlah')->get() as $key => $ob)<option value="{{$ob->obat_id}}">{{$ob->nama}}</option>@endforeach</select>');
        console.log('babi');
        $('.selectz2').select2({
            placeholder: 'Pilih',
            allowClear: true
        });
        $(".selectz2").on("select2:select", function (e) {
                var lastSelectedId = e.params.data.id;
                var lastSelectedName = e.params.data.text;
                $.ajax({
                    url: "{{ url('obat/haha') }}/" + lastSelectedId,
                    type: "GET",
                    dataType: "html",
                    success: function (result) {
                        var datas = JSON.parse(result);
                        $('.dosis').html('');
                        var duplicate = '<div class="row" id="obat'+lastSelectedId+'"> <div class="col-md-6"> <div class="form-group">  <input type="text" name="" value="'+datas.nama+'" readonly class="form-control"> </div> </div> <div class="col-md-6"> <div class="form-group"> <input type="text" required name="dosis['+lastSelectedId+']" value="" class="form-control" placeholder="Dosis"> </div> </div> </div>';
                        $(".dosis").append(duplicate);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal!", "Silahkan coba lagi", "error");
                    }
                });
            });

            $(".selectz2").on("select2:unselect", function (e) {
                        var lastSelectedId = e.params.data.id;
                        $('#obat'+lastSelectedId).remove();
                    });
    }
    else {
        $('.selectz2').html('');
        $('.dosis').html('');
    }
});
</script>
@endsection
