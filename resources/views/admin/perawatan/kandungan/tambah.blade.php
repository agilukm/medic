@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Imunisasi</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <hr>
            </div>
          </p>
          <div class="form-group">
              <form class="forms-sample" method="post" action="{{url('/perawatan/Imunisasi/tambah')}}">
                <div class="form-group">
                  <label for="exampleInputName1">Nama Bayi</label>
                  <input type="text" name="nama_bayi" class="form-control" placeholder="Nama Bayi" readonly value="{{$imunisasi->nama_bayi}}">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" name="pasien_id" class="form-control" value="{{$imunisasi->pasien_id}}">
                  <input type="hidden" name="pendaftaran_id" class="form-control" value="{{$pendaftaran_id}}">
                  <input type="hidden" name="tipe" class="form-control" value="lama">

                </div>

                <div class="form-group">
                  <label for="exampleInputPassword4">Tanggal Lahir</label>
                  <input required type="date" class="form-control" id="tgllahir" name="tgl_lahir" readonly placeholder="Tanggal Lahir" value="{{$imunisasi->tgl_lahir}}" max="<?php echo date('Y-m-d')?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail3">Jenis Kelamin</label>
                  <input required type="number" readonly class="form-control" id="noktp" readonly placeholder="Jenis Kelamin" name="jenis_kelamin" value="{{$imunisasi->jenis_kelamin}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail3">Umur</label>
                  <input required type="text" readonly class="form-control" id="kodepasien" placeholder="Umur" value="{{$imunisasi->umur}}" name="umur">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail3">Berat Badan</label>
                  <input required type="text" class="form-control" id="alamat" placeholder="Berat Badan" value="{{$imunisasi->berat_badan}}" name="berat_badan">
                </div>
                <label for="exampleInputPassword4">Jenis Imunisasi</label>
                  <select required class="form-control" name="jenis_imunisasi">
                      <option value="">Pilih</option>
                      <option value="Kb">KB</option>
                      <option value="Kandungan">Kandungan</option>
                      <option value="Lahiran">Lahiran</option>
                      <option value="Imunisasi">Imunisasi</option>
                  </select>
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <button class="btn btn-light" type="reset">Reset</button>
              </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('plugin')
@endsection
