@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pemeriksaan Kehamilan / Kandungan</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Anamnesis</th>
                  <th>GPA</th>
                  <th>Berat Badan</th>
                  <th>Tekanan Darah</th>
                  <th>TFU</th>
                  <th>LLA</th>
                  <th>DJJ</th>
                  <th>Jumlah Janin</th>
                  <th>Presensati</th>
                  <th>Status Imunisasi</th>
                  <th>Tablet FE</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->pasien->nama_pasien}}</td>
                  <td>{{$da->anamnesis}}</td>
                  <td>{{$da->gpa}}</td>
                  <td>{{$da->berat_badan}}</td>
                  <td>{{$da->tekanan_darah}}</td>
                  <td>{{$da->tinggi_fundus_uteri}}</td>
                  <td>{{$da->lingkar_lengan_atas}}</td>
                  <td>{{$da->denyut_jantung_janin}}</td>
                  <td>{{$da->jumlah_janin}}</td>
                  <td>{{$da->presentasi}}</td>
                  <td>{{$da->status_imunisasi}}</td>
                  <td>{{$da->tablet_fe}}</td>
                  <td>{{$da->created_at->format('d-m-Y')}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                            <th>No</th>
                            <th>Nama Jasa</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama Jasa</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
@endsection
