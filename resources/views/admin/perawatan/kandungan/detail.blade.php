@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pemeriksaan Kandungan {{$pasien->nama_pasien}}</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Anamnesis</th>
                    <th>GPA</th>
                    <th>Berat Badan</th>
                    <th>Tekanan Darah</th>
                    <th>TFU</th>
                    <th>LLA</th>
                    <th>DJJ</th>
                    <th>Jumlah Janin</th>
                    <th>Presensati</th>
                    <th>Status Imunisasi</th>
                    <th>Tablet FE</th>
                    <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                @foreach($detail_pasien as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->pasien->nama_pasien}}</td>
                  <td>{{$da->anamnesis}}</td>
                  <td>{{$da->gpa}}</td>
                  <td>{{$da->berat_badan}}</td>
                  <td>{{$da->tekanan_darah}}</td>
                  <td>{{$da->tinggi_fundus_uteri}}</td>
                  <td>{{$da->lingkar_lengan_atas}}</td>
                  <td>{{$da->denyut_jantung_janin}}</td>
                  <td>{{$da->jumlah_janin}}</td>
                  <td>{{$da->presentasi}}</td>
                  <td>{{$da->status_imunisasi}}</td>
                  <td>{{$da->tablet_fe}}</td>
                  <td>{{$da->created_at->format('d-m-Y')}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:1080px; right:50%;">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <h1>Detail Kandungan</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Umur</th>
                            <th>Jenis Kelamin</th>
                            <th>Berat Badan</th>
                            <th>Kandungan yang diberikan</th>
                            <th>Tanggal</th>
                          </tr>
                        </thead>
                        <tbody id="isidetail">

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <h2>Tambah Data Periksa Kandungan</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="forms-sample" method="post" action="{{url('/perawatan/Kandungan/tambah')}}">
                              <div class="form-group">
                                <label for="exampleInputName1">Anamnesis</label>
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <input type="text" name="anamnesis" class="form-control" placeholder="Anamnesis">
                                <input type="hidden" name="pasien_id" class="form-control" value="{{$pasien->id}}">
                                <input type="hidden" name="pendaftaran_id" class="form-control" value="{{$pendaftaran_id}}">

                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">GPA</label>
                                <input required type="text" class="form-control" name="gpa" id="kodepasien" placeholder="GPA">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Berat Badan</label>
                                <input required type="number" step ="0.01" name="berat_badan" class="form-control" id="noktp" placeholder="Berat Badan">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Tekanan Darah</label>
                                <input required type="text" class="form-control" name="tekanan_darah" id="alamat" placeholder="Tekanan Darah">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Tinggi Fundus Uteri</label>
                                <input required type="number" class="form-control" id="tgllahir" name="tinggi_fundus_uteri" placeholder="Tinggi Fundus Uteri">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Lingkar Lengan Atas</label>
                                <input required type="number" name="lingkar_lengan_atas" class="form-control" id="tgllahir" placeholder="Lingkar Lengan Atas">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Denyut Jantung Janin</label>
                                <input required type="number" name="denyut_jantung_janin" class="form-control" id="tgllahir" placeholder="Denyut Jantung Janin">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Jumlah Janin</label>
                                <input required type="text" name="jumlah_janin" class="form-control" id="tgllahir" placeholder="Jumlah Janin">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Presentasi</label>
                                <input required type="text" class="form-control" id="tgllahir" placeholder="Presentasi" name="presentasi">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Status Imunisasi</label>
                                <input required type="text" class="form-control" id="tgllahir" placeholder="Status Imunisasi" name="status_imunisasi">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Tablet FE</label>
                                <input required type="string" class="form-control"  placeholder="Table FE" name="tablet_fe">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Tanggal Kembali</label>
                                <input required type="date" value="<?php
                                     echo date('Y-m-d', strtotime("+1 month"));
                                 ?>"  class="form-control" name="tgl_kembali" id="tgllahir" placeholder="Tanggal Kembali" min=
     <?php
        echo date('Y-m-d', strtotime("+1 day"));
     ?>>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Obat</label>
                                <select class="form-control select2 " name="obat_id[]" multiple style="width:100%">
                                    <option value="">Pilih</option>
                                    @foreach(\DB::table('obat')->leftjoin('obat_pemeriksaan','obat.id', '=', 'obat_id')->where('stok', '>=', 1)->where('jenis', 'Kandungan')->get() as $key => $ob)
                                        <option value="{{$ob->obat_id}}">{{$ob->nama}}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Dosis</label>
                                <div class="dosis">

                                </div>
                              </div>
                              <button type="submit" class="btn btn-success mr-2">Submit</button>
                              <button class="btn btn-light" type="reset">Reset</button>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
<script>
function detail(id, nama_bayi) {
    $.ajax({
            url: "{{ url('perawatan/Kandungan/detail/readByPasienAll') }}/" + id + '/' + nama_bayi,
            type: "GET",
            dataType: "html",
            success: function (result) {
                $("#isidetail").html('');
                var datas = JSON.parse(result);
                $.each(datas, function(k, data) {
                    var i = 1;
                    $('#isidetail').append(
                        $("<tr>"),
                        $("<td>").text(i),
                        $("<td>").text(data.umur),
                        $("<td>").text(data.jenis_kelamin),
                        $("<td>").text(data.berat_badan),
                        $("<td>").text(data.jenis_imunisasi),
                        $("<td>").text(data.created_at),
                        $("</tr>")
                    );
                    i++;
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
};
$(".select2").on("select2:select", function (e) {
        var lastSelectedId = e.params.data.id;
        var lastSelectedName = e.params.data.text;
        $.ajax({
            url: "{{ url('obat/haha/') }}/" + lastSelectedId,
            type: "GET",
            dataType: "html",
            success: function (result) {
                var datas = JSON.parse(result);
                var duplicate = '<div class="row" id="obat'+lastSelectedId+'"> <div class="col-md-6"> <div class="form-group">  <input type="text" name="" value="'+datas.nama+'" readonly class="form-control"> </div> </div> <div class="col-md-6"> <div class="form-group"> <input type="text" required name="dosis['+lastSelectedId+']" value="" class="form-control" placeholder="Dosis"> </div> </div> </div>';
                $(".dosis").append(duplicate);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
    });

    $(".select2").on("select2:unselect", function (e) {
                var lastSelectedId = e.params.data.id;
                $('#obat'+lastSelectedId).remove();
            });
</script>
@endsection
