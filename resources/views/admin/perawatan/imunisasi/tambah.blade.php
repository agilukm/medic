@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Imunisasi</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <hr>
            </div>
          </p>
          <div class="form-group">
              <form class="forms-sample" method="post" action="{{url('/perawatan/Imunisasi/tambah')}}">
                <div class="form-group">
                  <label for="exampleInputName1">Nama Bayi</label>
                  <input type="text" name="nama_bayi" class="form-control" placeholder="Nama Bayi" readonly value="{{$imunisasi->nama_bayi}}">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" name="pasien_id" class="form-control" value="{{$imunisasi->pasien_id}}">
                  <input type="hidden" name="pendaftaran_id" class="form-control" value="{{$pendaftaran_id}}">
                  <input type="hidden" name="tipe" class="form-control" value="lama">

                </div>

                <div class="form-group">
                  <label for="exampleInputPassword4">Tanggal Lahir</label>
                  <input required type="date" class="form-control" id="tgllahir" name="tgl_lahir"  min="2000-01-01"  readonly placeholder="Tanggal Lahir" value="{{$imunisasi->tgl_lahir}}" max="<?php echo date('Y-m-d')?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail3">Jenis Kelamin</label>
                  <input required type="text" readonly class="form-control" id="noktp" readonly placeholder="Jenis Kelamin" name="jenis_kelamin" value="{{$imunisasi->jenis_kelamin}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail3">Umur</label>
                  <input required type="text" readonly class="form-control" id="kodepasien" placeholder="Umur" value="{{$imunisasi->umur}}" name="umur">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail3">Berat Badan</label>
                  <input required type="number" step="0.01" class="form-control" id="alamat" placeholder="Berat Badan" value="{{$imunisasi->berat_badan}}" name="berat_badan" min="0">
                </div>
                <div class="form-group">
                <label for="exampleInputPassword4">Jenis Imunisasi</label>
                <select required class="form-control" name="jenis_imunisasi">
                                    <option value="">Pilih</option>
                                    <option value="BCG">BCG</option>
                                    <option value="DPT PENTA BIO 1">DPT PENTA BIO 1</option>
                                    <option value="DPT PENTA BIO 2">DPT PENTA BIO 2</option>
                                    <option value="DPT PENTA BIO 3">DPT PENTA BIO 3</option>
                                    <option value="CAMPAK / RUBELLA">CAMPAK / RUBELLA</option>
                                    <option value="POLIO">POLIO</option>
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword4">Tanggal Kembali</label>
                              <input required type="date" value="<?php
                                   echo date('Y-m-d', strtotime("+1 month"));
                               ?>" class="form-control" name="tgl_kembali" id="tgllahir" placeholder="Tanggal Kembali" min=
   <?php
      echo date('Y-m-d', strtotime("+1 day"));
   ?>>
                            </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword4">Obat</label>
                                  <select class="form-control select2 " name="obat_id[]" multiple style="width:100%">
                                      <option value="">Pilih</option>
                                      @foreach(\DB::table('obat')->leftjoin('obat_pemeriksaan','obat.id', '=', 'obat_id')->where('stok', '>=', 1)->where('jenis', 'imunisasi')->get() as $key => $ob)
                                          <option value="{{$ob->obat_id}}">{{$ob->nama}}</option>
                                      @endforeach
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputPassword4">Dosis</label>
                                  <div class="dosis">

                                  </div>
                                </div>
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <button class="btn btn-light" type="reset">Reset</button>
              </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('plugin')
<script type="text/javascript">
$(".select2").on("select2:select", function (e) {
        var lastSelectedId = e.params.data.id;
        var lastSelectedName = e.params.data.text;
        $.ajax({
            url: "{{ url('obat/haha/') }}/" + lastSelectedId,
            type: "GET",
            dataType: "html",
            success: function (result) {
                var datas = JSON.parse(result);
                $('.dosis').html('');
                var duplicate = '<div class="row" id="obat'+lastSelectedId+'"> <div class="col-md-6"> <div class="form-group">  <input type="text" name="" value="'+datas.nama+'" readonly class="form-control"> </div> </div> <div class="col-md-6"> <div class="form-group"> <input type="text" required name="dosis['+lastSelectedId+']" value="" class="form-control" placeholder="Dosis"> </div> </div> </div>';
                $(".dosis").append(duplicate);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
    });

    $(".select2").on("select2:unselect", function (e) {
                var lastSelectedId = e.params.data.id;
                $('#obat'+lastSelectedId).remove();
            });
</script>
@endsection
