@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Imunisasi {{$pasien->nama_pasien}}</h4>
          <p>Pilih atau tambah anak yang akan di imunisasi</p>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah Anak</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Orang Tua</th>
                  <th>Nama Bayi</th>
                  <th>Umur</th>
                  <th>Jenis Kelamin</th>
                  <th>Berat Badan</th>
                  <th>Imunisasi yang diberikan</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($detail_pasien as $key => $detail)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$detail->pasien->nama_pasien}}</td>
                  <td>{{$detail->nama_bayi}}</td>
                  <td>{{$detail->umur}}</td>
                  <td>{{$detail->jenis_kelamin}}</td>
                  <td>{{$detail->berat_badan}}</td>
                  <td>{{$detail->jenis_imunisasi}}</td>
                  <td>{{$detail->created_at}}</td>
                  <td>
                    <!-- <label class="badge badge-danger">Pending</label> -->
                     <a href="#" class="btn btn-info" data-toggle="modal" data-target="#detailModal" onclick="detail({{$detail->pasien_id}}, '{{$detail->nama_bayi}}')">Detail</a> <br>
                     <a href="{{ url('/perawatan/Imunisasi/tambah/'.$detail->id.'/'.$pendaftaran_id)}}" class="btn btn-info" >Pilih</a> <br>
                  </td>
                  @endforeach
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="width:1080px; right:50%;">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <h1>Detail Imunisasi</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Umur</th>
                            <th>Jenis Kelamin</th>
                            <th>Berat Badan</th>
                            <th>Imunisasi yang diberikan</th>
                            <th>Tanggal</th>
                          </tr>
                        </thead>
                        <tbody id="isidetail">

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <h2>Tambah Anak</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="forms-sample" method="post" action="{{url('/perawatan/Imunisasi/tambah')}}">
                              <div class="form-group">
                                <label for="exampleInputName1">Nama Anak</label>
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <input type="text" name="nama_bayi" class="form-control">
                                <input type="hidden" name="pasien_id" class="form-control" value="{{$pasien->id}}">
                                <input type="hidden" name="pendaftaran_id" class="form-control" value="{{$pendaftaran_id}}">
                                <input type="hidden" name="tipe" class="form-control" value="baru">

                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">Tanggal Lahir</label>
                                <input required type="date" class="form-control" id="tgllahir" name="tgl_lahir" placeholder="Tanggal Lahir" min="2000-01-01" max="<?php
         echo date('Y-m-d')?>">
                              </div>

                              <div class="form-group">
                                <label for="exampleInputEmail3">Umur</label>
                                <input required type="text" readonly class="form-control" name="umur" id="umur" placeholder="Umur">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Jenis Kelamin</label>
                                <select required type="text" name="jenis_kelamin"  class="form-control">
                                    <option value="">Pilih</option>
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail3">Berat Badan</label>
                                <input required type="number" step ="0.01" class="form-control" name="berat_badan" id="alamat" placeholder="Berat Badan" min="0">
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword4">Jenis Imunisasi</label>
                                <select required class="form-control" name="jenis_imunisasi">
                                    <option value="">Pilih</option>
                                    <option value="BCG">BCG</option>
                                    <option value="DPT PENTA BIO 1">DPT PENTA BIO 1</option>
                                    <option value="DPT PENTA BIO 2">DPT PENTA BIO 2</option>
                                    <option value="DPT PENTA BIO 3">DPT PENTA BIO 3</option>
                                    <option value="CAMPAK / RUBELLA">CAMPAK / RUBELLA</option>
                                    <option value="POLIO">POLIO</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Tanggal Kembali</label>
                                <input required type="date" value="<?php
                                     echo date('Y-m-d', strtotime("+1 month"));
                                 ?>" readonly class="form-control" name="tgl_kembali" id="tgllahir" placeholder="Tanggal Kembali" min=
     <?php
        echo date('Y-m-d', strtotime("+1 day"));
     ?>>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Obat</label>
                                <select class="form-control select2 " name="obat_id[]" multiple style="width:100%">
                                    <option value="">Pilih</option>
                                    @foreach(\DB::table('obat')->leftjoin('obat_pemeriksaan','obat.id', '=', 'obat_id')->where('stok', '>=', 1)->where('jenis', 'imunisasi')->get() as $key => $ob)
                                        <option value="{{$ob->obat_id}}">{{$ob->nama}}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword4">Dosis</label>
                                <div class="dosis">

                                </div>
                              </div>
                              <button type="submit" class="btn btn-success mr-2">Submit</button>
                              <button class="btn btn-light" type="reset">Reset</button>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
<script>
$("#tgllahir").change(function () {
    var input = $("#tgllahir").val();
    s = moment(input, 'YYYY-M-D');
    n = moment();

    year = n.diff(s, 'years');
    month = n.diff(s, 'months')
    day = n.diff(s, 'days')
    tahun = '';
    bulan = '';
    hari = '';
    if(year != 0)
    {
        tahun = year+' tahun';
    }
    if(month != 0)
    {
        bulan = ' '+month+' bulan';
    }
    if(month == 0)
    {
        hari = ' '+day+' hari';
    }

    $("#umur").val(tahun+bulan+hari);
})

function detail(id, nama_bayi) {
    $.ajax({
            url: "{{ url('perawatan/Imunisasi/detail/readByPasienAll') }}/" + id + '/' + nama_bayi,
            type: "GET",
            dataType: "html",
            success: function (result) {
                $("#isidetail").html('');
                var datas = JSON.parse(result);
                $.each(datas, function(k, data) {
                    var i = 1;
                    $('#isidetail').append(
                        $("<tr>"),
                        $("<td>").text(i),
                        $("<td>").text(data.umur),
                        $("<td>").text(data.jenis_kelamin),
                        $("<td>").text(data.berat_badan),
                        $("<td>").text(data.jenis_imunisasi),
                        $("<td>").text(data.created_at),
                        $("</tr>")
                    );
                    i++;
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
};
        $(".select2").on("select2:select", function (e) {
                var lastSelectedId = e.params.data.id;
                var lastSelectedName = e.params.data.text;
                $.ajax({
                    url: "{{ url('obat/haha/') }}/" + lastSelectedId,
                    type: "GET",
                    dataType: "html",
                    success: function (result) {
                        var datas = JSON.parse(result);
                        var duplicate = '<div class="row" id="obat'+lastSelectedId+'"> <div class="col-md-6"> <div class="form-group">  <input type="text" name="" value="'+datas.nama+'" readonly class="form-control"> </div> </div> <div class="col-md-6"> <div class="form-group"> <input type="text" required name="dosis['+lastSelectedId+']" value="" class="form-control" placeholder="Dosis"> </div> </div> </div>';
                        $(".dosis").append(duplicate);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                            swal("Gagal!", "Silahkan coba lagi", "error");
                    }
                });
            });

            $(".select2").on("select2:unselect", function (e) {
                        var lastSelectedId = e.params.data.id;
                        $('#obat'+lastSelectedId).remove();
                    });

</script>
</script>
@endsection
