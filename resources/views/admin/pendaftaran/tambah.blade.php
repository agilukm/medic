@extends('admin.layout')

@section('content')

<div class="content-wrapper">
  <div class="row">
      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Pendaftaran</h4>
            <p class="card-description">
              Pilih Tipe Pasien
            </p>
            <select class="select2 form-control" name="" id="tipepasien">
                <option value="">Pilih</option>
                <option value="baru">Baru</option>
                <option value="lama">Lama</option>
            </select>
        </div>
    </div>
</div>
</div>
      <div class="" id="baru" style="display:none">
          <div class="row">
              <div class="col-md-12 grid-margin stretch-card">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card col-md-12">
                <div class="card-body">
                  <h4 class="card-title">Input Data Pasien</h4>
                  <p class="card-description">
                    Data Pasien
                  </p>
                  <form class="forms-sample" action="{{url('/pendaftaran/tambah')}}" method="post">
                    <div class="form-group">
                      <label for="exampleInputName1">Nama</label>
                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      <input required type="hidden" class="form-control" id="" placeholder="type" name="tipe_pasien" value="baru">
                      <input required type="text" class="form-control" id="" name="nama" placeholder="Nama" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Nama Suami</label>
                      <input required type="text" class="form-control" id="" placeholder="Nama Suami" name="nama_suami">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">No KTP</label>
                      <input required type="number" class="form-control" id="" placeholder="No KTP" name="ktp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Alamat</label>
                      <input required type="text" class="form-control" id="" placeholder="Alamat" name="alamat">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword4">Tanggal Lahir</label>
                      <input required type="date" class="form-control" id="" placeholder="Tanggal Lahir" name="tgl_lahir">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword4">Tipe Pemeriksaan</label>
                      <select required class="form-control" name="tipe_perawatan">
                          <option value="">Pilih</option>
                          <option value="Kb">KB</option>
                          <option value="Kandungan">Kandungan</option>
                          <option value="Lahiran">Lahiran</option>
                          <option value="Imunisasi">Imunisasi</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <button class="btn btn-light" type="reset">Reset</button>
                  </form>
                </div>
              </div>
            </div>
            </div>
            </div>
        </div>
        <div class="" id="lama" style="visibility:hidden">
              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Pilih Pasien</h4>
                    <p class="card-description">
                      Data Pasien
                    </p>
                    <form class="forms-sample" method="post" action="{{url('/pendaftaran/tambah')}}">
                      <div class="form-group">
                        <label for="exampleInputName1">Nama</label>
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input required type="hidden" class="form-control" id="" placeholder="type" name="tipe_pasien" value="lama">
                        <select required class="select2 form-control" name="pasien_id" id="pasien_id">
                            <option value="">Pilih</option>
                            @foreach($pasien as $pas)
                                <option value="{{$pas->id}}">{{$pas->nama_pasien}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Kode Pasien</label>
                        <input required type="text" readonly class="form-control" id="kodepasien" placeholder="Kode Pasien">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Nama Suami</label>
                        <input required type="text" readonly class="form-control" id="nama_suami" placeholder="Nama Suami">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">No KTP</label>
                        <input required type="number" readonly class="form-control" id="noktp" placeholder="No KTP">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Alamat</label>
                        <input required type="text" class="form-control" id="alamat" placeholder="Alamat">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4">Tanggal Lahir</label>
                        <input required type="date" class="form-control" id="tgllahir" placeholder="Tanggal Lahir">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4">Tipe Pemeriksaan</label>
                        <select required class="form-control" name="tipe_perawatan">
                            <option value="">Pilih</option>
                            <option value="Kb">KB</option>
                            <option value="Kandungan">Kandungan</option>
                            <option value="Lahiran">Lahiran</option>
                            <option value="Imunisasi">Imunisasi</option>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Reset</button>
                    </form>
                  </div>
                </div>
              </div>
          </div>
  </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection
@section('plugin')
<script type="text/javascript">
$("#tipepasien").change(function() {

    $("#kodepasien").val();
    $("#noktp").val();
    $("#alamat").val();
    $("#tgllahir").val();

    if ($('#tipepasien').val() == 'baru') {
        document.getElementById("lama").style.visibility = "hidden";
        $('#baru').show("slow");
    }
    else{
        document.getElementById("lama").style.visibility = "visible";
        $('#baru').hide("slow");
    }
})

$("#pasien_id").change(function() {
    $.ajax({
            url: "{{ url('pasien/') }}/" + $(this).val(),
            type: "GET",
            dataType: "html",
            success: function (result) {
                var datas = JSON.parse(result);
                    $("#kodepasien").val(datas.kode_pasien);
                    $("#noktp").val(datas.ktp);
                    $("#alamat").val(datas.alamat);
                    $("#nama_suami").val(datas.nama_suami);
                    $("#tgllahir").val(datas.tgl_lahir);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Silahkan coba lagi", "error");
            }
        });
});
</script>
@endsection
