@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pendaftar</h4>
          <p class="card-description">
            <div class="right" style="float:left">
                <div class="row">
                          <div class="col-md-12">
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Tanggal </label>
                              <div class="col-sm-8">
                                <input type="date" name="" id="tglhe" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div>


        </div>
                <hr>
            <div class="right" style="float:right">
                <a href="{{url('/pendaftaran/tambah')}}">
                    <button class="btn btn-primary" type="button" name="button">Daftar</button>
                </a>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTables">
              <thead>
                <tr>
                  <th width="1%">No</th>
                  <th>Kode Registrasi</th>
                  <th>Nama Pasien</th>
                  <th>Jenis Pemeriksaan</th>
                  <th>Jam Masuk</th>
                  <th>Jam Proses</th>
                  <th>Jam Selesai</th>
                  <th>Status</th>
                  <th>Aksi</th>
                  <th style="display:none"></th>
                </tr>
              </thead>
              <tbody>
              @foreach($pendaftarans as $key => $pendaftaran)
                <tr>
                  <td width="1%" align="center">{{++$key}}</td>
                  <td>{{$pendaftaran->kode_registrasi}}</td>
                  <td>{{$pendaftaran->pasien->nama_pasien}}</td>
                  <td>{{$pendaftaran->perawatan}}</td>
                  <td>{{date('d-m-Y H:i:s', strtotime($pendaftaran->waktu_daftar))}}</td>
                  <td>{{date('d-m-Y H:i:s', strtotime($pendaftaran->waktu_masuk))}}</td>
                  <td>{{isset($pendaftaran->waktu_keluar) ? date('d-m-Y H:i:s', strtotime($pendaftaran->waktu_keluar)) : '-'}}</td>
                  <td><label class="badge badge-info">{{$pendaftaran->statusLabel()}}</label></td>
                  <td>
                      @if($pendaftaran->status == 0)
                        <a href="{{ url('/pendaftaran/proses/1/'.$pendaftaran->id) }}">Proses</a>
                      @elseif($pendaftaran->status==1)
                        <a href="{{ url('/pendaftaran/detail/1/'.$pendaftaran->id) }}">Detail</a>
                      @elseif($pendaftaran->status==2)
                        <a href="{{ url('/pembayaran/registrasi/'.$pendaftaran->id) }}">Bayar</a>
                      @endif
                         <br>
                    <!-- <label class="badge badge-danger">Pending</label> -->
                    <!-- Hapus -->
                  </td>
                  <td style="display:none"><span style="display:none">{{$pendaftaran->waktu_daftar}}</span></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection
@section('plugin')
<script type="text/javascript">
    var tables = $('#myTables').DataTable({
         // "oSearch": {"sSearch": "<?php echo date('Y-m-d') ?>"}
    });
    $('#tglhe').on('change', function(){
        tables.column( 9 )
                    .search( $(this).val() )
                    .draw();
    });
</script>
@endsection
