@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pembiayaan</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button>
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Jenis Pemeriksaan</th>
                  <th>Detail</th>
                  <th>Harga</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $key => $da)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$da->jenis}}</td>
                  <td>{{$da->detail}}</td>
                  <td>Rp {{number_format($da->harga, 2, ',', '.')}}</td>
                  <td>
                      <a href="{{ url('/master/biaya/edit/'.$da->id)}}" class="btn btn-info">Edit</a> <br>
                      <a href="{{ url('/master/biaya/hapus/'.$da->id)}}" class="btn btn-danger">Hapus</a> <br>
                  </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="modalTambah">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="forms-sample" method="post" action="{{url('/master/biaya')}}">
                      <div class="form-group">
                        <label for="exampleInputName1">Jenis Perawatan</label>
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <select required class="form-control" name="jenis_perawatan">
                            <option value="">Pilih</option>
                            <option value="Kb">Kb</option>
                            <option value="Kandungan">Kandungan</option>
                            <option value="Imunisasi">Imunisasi</option>
                            <option value="Lahiran">Lahiran</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail3">Detail</label>
                        <input required type="text" class="form-control" name="detail" placeholder="Detail">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail3">Harga</label>
                        <input required type="number" min="1" class="form-control" name="harga" placeholder="Harga">
                      </div>

                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Reset</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

@endsection
@section('plugin')
@endsection
