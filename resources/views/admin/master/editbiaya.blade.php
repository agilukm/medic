@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pembiayaan</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <form class="forms-sample" method="post" action="{{url('/master/biaya/edit/'.$data->id)}}">
            <div class="form-group">
              <label for="exampleInputName1">Jenis Perawatan</label>
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              <select required class="form-control" name="jenis_perawatan">
                  <option value="">Pilih</option>
                  <option value="Kb" @if($data->jenis == 'Kb') selected @endif >Kb</option>
                  <option value="Kandungan" @if($data->jenis == 'Kandungan') selected @endif>Kandungan</option>
                  <option value="Imunisasi" @if($data->jenis == 'Imunisasi') selected @endif>Imunisasi</option>
                  <option value="Lahiran" @if($data->jenis == 'Lahiran') selected @endif>Lahiran</option>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail3">Detail</label>
              <input required type="text" class="form-control" name="detail" placeholder="Detail" value="{{$data->detail}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Harga</label>
              <input required type="number" class="form-control" name="harga" placeholder="Harga" value="{{$data->harga}}">
            </div>

            <button type="submit" class="btn btn-success mr-2">Submit</button>
            <button class="btn btn-light" type="reset">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection
@section('plugin')
@endsection
