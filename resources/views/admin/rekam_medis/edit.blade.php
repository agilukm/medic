@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pasien</h4>
          <form class="forms-sample" method="post" action="{{url('/rekam_medis/simpan/'.$data->id)}}">
              <div class="form-group">
                <label for="exampleInputEmail3">Kode</label>
                <input required type="text" readonly class="form-control" name=""  value="{{$data->kode_pasien}}">
              </div>
            <div class="form-group">
              <label for="exampleInputName1">Nama</label>
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              <input type="text" name="nama_pasien" placeholder="Nama" class="form-control" value="{{$data->nama_pasien}}" required>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail3">Nama Suami</label>
              <input required type="text" class="form-control" name="nama_suami" placeholder="Nama Suami" value="{{$data->nama_suami}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Alamat</label>
              <input required type="text" class="form-control" name="alamat" placeholder="alamat" required value="{{$data->alamat}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Tanggal Lahir</label>
              <input required type="date"  class="form-control" name="tgl_lahir" placeholder="Harga Jual" value="{{$data->tgl_lahir}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">No. KTP</label>
              <input required type="text" class="form-control" name="ktp" placeholder="No. KTP" value="{{$data->ktp}}">
            </div>

            <button type="submit" class="btn btn-success mr-2">Submit</button>
            <button class="btn btn-light" type="reset">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection
@section('plugin')
@endsection
