@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Pasien / Rekam Medis</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Nama Suami</th>
                  <th>Alamat</th>
                  <th>Tanggal Lahir</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($data as $key => $d)
                <tr>
                  <td>{{++$key}}</td>
                  <td>{{$d->nama_pasien}}</td>
                  <td>{{$d->nama_suami}}</td>
                  <td>{{$d->alamat}}</td>
                  <td>{{date('d-m-Y', strtotime($d->tgl_lahir))}}</td>
                  <td>
                      <a href="{{ url('/rekam_medis/edit/'.$d->id) }}" class="btn btn-primary">Edit</a>
                      <a href="{{ url('/rekam_medis/rekam/'.$d->id) }}" class="btn btn-primary">Rekam Medis</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal">
          <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                            <th>No</th>
                            <th>Nama Jasa</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->

        <!-- content-wrapper ends -->
        <div class="modal fade" id="modalTambah">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Nama Jasa</th>
                                    <th>Jumlah</th>
                                    <th>Satuan</th>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                  </div>
                </div>
                <!--  End Modal -->

@endsection
@section('plugin')
@endsection
