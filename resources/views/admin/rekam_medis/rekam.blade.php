@extends('admin.layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Rekam Medis</h4>
          <p class="card-description">
            <div class="right" style="float:right">
                <!-- <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#modalTambah">Tambah</button> -->
                <hr>
            </div>
          </p>
          <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Pilihan Perawatan</h4>
                    <div class="vertical-tab">
                      <ul class="nav nav-tabs tab-solid tab-solid-info mr-4" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link" id="tab-6-1" data-toggle="tab" href="#contact-6-1" role="tab" aria-controls="contact-6-1" aria-selected="false">Imunisasi</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="tab-6-2" data-toggle="tab" href="#contact-6-2" role="tab" aria-controls="contact-6-2" aria-selected="false">Kandungan</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link " id="tab-6-3" data-toggle="tab" href="#contact-6-3" role="tab" aria-controls="contact-6-3" aria-selected="false">Lahiran</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link"  id="tab-6-4" data-toggle="tab" href="#contact-6-4" role="tab" aria-controls="contact-6-4" aria-selected="false">KB</a>
                        </li>
                      </ul>
                      <div class="tab-content tab-content-solid">
                        <div class="tab-pane fade" id="contact-6-1" role="tabpanel" aria-labelledby="tab-6-1">
                            <!-- Imunisasi -->
                            <br>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Tanggal</td>
                                        <td>Nama Bayi</td>
                                        <td>Jenis Kelamin</td>
                                        <td>Tanggal Lahir</td>
                                        <td>Detail</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($imunisasi as $key => $data)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$data->created_at->format('d-m-Y')}}</td>
                                            <td>{{$data->nama_bayi}}</td>
                                            <td>{{$data->jenis_kelamin}}</td>
                                            <td>{{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                                            <td>
                                                 <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#detailModal{{$data->id}}">Lihat</button>
                                                 <a href="{{ url('/') }}/rekam_medis/imunisasi/{{$data->pasien_id}}/{{$data->nama_bayi}}"<button class="btn btn-primary" type="button">Cetak Kartu</button>
                                            </td>
                                        </tr>
<!-- content-wrapper ends -->
<div class="modal fade" id="detailModal{{$data->id}}">
          <div class="modal-dialog" role="document">
            <div class="modal-content"  style="display: block; width:150%">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div id="chartContainer{{$data->id}}" style="height: 370px; width: 100%;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
          </div>
        </div>
        <!--  End Modal -->


                                    @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <br>
                            <br>
                            <br>
                            <center>
                                <div id="chartContainer" style="height: 370px; width: 100%; margin-left:150px;"></div>

                        </div>
                        <div class="tab-pane fade" id="contact-6-2" role="tabpanel" aria-labelledby="tab-6-2">
                            <div class="table-responsive">
                            <br>
                            <table class="table" id="">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Nama</th>
                                  <th>Anamnesis</th>
                                  <th>GPA</th>
                                  <th>Berat Badan</th>
                                  <th>Tekanan Darah</th>
                                  <th>TFU</th>
                                  <th>LLA</th>
                                  <th>DJJ</th>
                                  <th>Jumlah Janin</th>
                                  <th>Presensati</th>
                                  <th>Status Imunisasi</th>
                                  <th>Tablet FE</th>
                                  <th>Tanggal</th>
                                  <th>Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($kandungan as $key => $da)
                                <tr>
                                  <td>{{++$key}}</td>
                                  <td>{{$da->pasien->nama_pasien}}</td>
                                  <td>{{$da->anamnesis}}</td>
                                  <td>{{$da->gpa}}</td>
                                  <td>{{$da->berat_badan}}</td>
                                  <td>{{$da->tekanan_darah}}</td>
                                  <td>{{$da->tinggi_fundus_uteri}}</td>
                                  <td>{{$da->lingkar_lengan_atas}}</td>
                                  <td>{{$da->denyut_jantung_janin}}</td>
                                  <td>{{$da->jumlah_janin}}</td>
                                  <td>{{$da->presentasi}}</td>
                                  <td>{{$da->status_imunisasi}}</td>
                                  <td>{{$da->table_fe}}</td>
                                  <td>{{$da->created_at->format('d-m-Y')}}</td>
                                  <td><a href="{{ url('/') }}/rekam_medis/kandungan/{{$da->pasien_id}}"<button class="btn btn-primary" type="button">Cetak Kartu</button>
                             </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>
                        </div>
                        <div class="tab-pane fade  " id="contact-6-3" role="tabpanel" aria-labelledby="tab-6-3">
                            <br>
                            <table class="table" id="">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Nama Orang Tua</th>
                                  <th>Umur</th>
                                  <th>Jam Lahir</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Berat Badan</th>
                                  <th>PB</th>
                                  <th>Therapi</th>
                                  <th>Lama Perawatan</th>
                                  <th>Tanggal</th>
                                </tr>
                              </thead>
                              <tbody>
                                  @foreach($lahiran as $key => $d)
                                <tr>
                                  <td>{{++$key}}</td>
                                  <td>{{$d->pasien->nama_pasien}}</td>
                                  @php
                                    $to = \Carbon\Carbon::now();
                                    $from = \Carbon\Carbon::createFromFormat('Y-m-d', $d->pasien->tgl_lahir);
                                   @endphp
                                  <td>{{$to->diffInYears($from)}}</td>
                                  <td>{{date('d-m-Y H:i:s', strtotime($d->jam_lahir))}}</td>
                                  <td>{{$d->jenis_kelamin}}</td>
                                  <td>{{$d->berat_badan}}</td>
                                  <td>{{$d->pb}}</td>
                                  <td>{{$d->therapi}}</td>
                                  @php
                                  $diff = '-';
                                  $registrasi = \DB::table('log_pemeriksaan')->join('registrasi', 'registrasi.id', '=', 'log_pemeriksaan.pendaftaran_id')->where('jenis', 'Lahiran')->where('jenis_id', $d->id)->first();
                                  if($registrasi){
                                  $from =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_masuk);
                                  $to =  \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $registrasi->waktu_keluar);
                                  $diff = $to->diffInDays($from);
                                  if($diff == 0)
                                  {
                                      $diff = 1;
                                  }
                                  }
                                  @endphp
                                  <td>{{$diff}} hari</td>
                                  <td>{{$d->created_at->format('d-m-Y')}}</td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade  " id="contact-6-4" role="tabpanel" aria-labelledby="tab-6-4">
                            <br>
                            <table class="table" id="">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Nama</th>
                                  <th>Berat Badan</th>
                                  <th>Tekanan Darah</th>
                                  <th>Tanggal</th>
                                  <th>Tanggal Kembali</th>
                                  <th>Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                                  @foreach($kb as $key => $d)
                                <tr>
                                  <td>{{++$key}}</td>
                                  <td>{{$d->pasien->nama_pasien}}</td>
                                  <td>{{$d->berat_badan}}</td>
                                  <td>{{$d->tekanan_darah}}</td>
                                  <td>{{$d->created_at->format('d-m-Y')}}</td>
                                  <td>{{date('d-m-Y', strtotime($d->tanggal_kembali))}}</td>
                                  <td><a href="{{ url('/') }}/rekam_medis/kb/{{$d->pasien_id}}"<button class="btn btn-primary" type="button">Cetak Kartu</button>
                             </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
@section('plugin')

<script src="{{url('/')}}/admin/jquery.canvasjs.min.js"></script>

<script>
@foreach($imunisasi as $key => $data)
var options{{$key}} = {
	animationEnabled: true,
	title:{
		text: "Perkembangan Berat Badan Imunisasi {{$data->nama_bayi}}"
	},
	axisY: {
		title: "Berat Badan",
		prefix: "",
		includeZero: false
	},
	data: [{
		yValueFormatString: "#,### Kg",
		xValueFormatString: "MMM YYYY",
		type: "line",
		dataPoints: [
            @foreach(\App\Services\Perawatan\Imunisasi\Imunisasi::where('nama_bayi', $data->nama_bayi)->where('pasien_id', $data->pasien_id)->get() as $key2 => $data2)
			{
                x: new Date({{$data2->created_at->format('Y')}}, {{$data2->created_at->format('m')-1}}), y: {{$data2->berat_badan}} },
            @endforeach
        ]
	}]
};
$("#chartContainer{{$data->id}}").CanvasJSChart(options{{$key}});
@endforeach
</script>
@endsection
