<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('pendaftaran')->group(function () {
    Route::get('/', 'PendaftaranController@index')->name('pendaftaran');
    Route::get('/tambah', 'PendaftaranController@tambah');
    Route::get('/proses/{status}/{id}', 'PendaftaranController@proses')->name('proses');
    Route::get('/detail/{status}/{id}', 'PendaftaranController@detail')->name('proses');
    Route::post('/tambah', 'PendaftaranController@daftar');
        // Matches The "/admin/users" URL
});

Route::prefix('perawatan')->group(function () {
    Route::prefix('Imunisasi')->group(function () {
        Route::get('/detail/{id}/{pendaftaran_id}', 'Perawatan\ImunisasiController@readByPasien');
        Route::get('/tambah/{id}/{pendaftaran_id}', 'Perawatan\ImunisasiController@tambahById');
        Route::post('/tambah', 'Perawatan\ImunisasiController@simpan');
        Route::get('/', 'Perawatan\ImunisasiController@index');
        Route::get('/detail/readByPasienAll/{id}/{nama_bayi}', 'Perawatan\ImunisasiController@readByPasienAll');
    });

    Route::prefix('Kb')->group(function () {
        Route::get('/detail/{id}/{pendaftaran_id}', 'Perawatan\KbController@readByPasien');
        Route::get('/tambah/{id}/{pendaftaran_id}', 'Perawatan\KbController@tambahById');
        Route::post('/tambah', 'Perawatan\KbController@simpan');
        Route::get('/', 'Perawatan\KbController@index');
        Route::get('/detail/readByPasienAll/{id}/{nama_bayi}', 'Perawatan\KbController@readByPasienAll');
    });

    Route::prefix('Lahiran')->group(function () {
        Route::get('/detail/{id}/{pendaftaran_id}', 'Perawatan\LahiranController@readByPasien');
        Route::get('/tambah/{id}/{pendaftaran_id}', 'Perawatan\LahiranController@tambahById');
        Route::post('/tambah', 'Perawatan\LahiranController@simpan');
        Route::get('/', 'Perawatan\LahiranController@index');
        Route::get('/detail/readByPasienAll/{id}/{nama_bayi}', 'Perawatan\LahiranController@readByPasienAll');
    });

    Route::prefix('Kandungan')->group(function () {
        Route::get('/detail/{id}/{pendaftaran_id}', 'Perawatan\KandunganController@readByPasien');
        Route::get('/tambah/{id}/{pendaftaran_id}', 'Perawatan\KandunganController@tambahById');
        Route::post('/tambah', 'Perawatan\KandunganController@simpan');
        Route::get('/', 'Perawatan\KandunganController@index');
        Route::get('/detail/readByPasienAll/{id}/{nama_bayi}', 'Perawatan\KandunganController@readByPasienAll');
    });
});

Route::prefix('pembayaran')->group(function () {
    Route::get('/', 'PembayaranController@index');
    Route::get('/registrasi/{no_registrasi}', 'PembayaranController@readByRegistrasi');
    Route::post('/bayar', 'PembayaranController@bayar');
    Route::get('/detail/{id}', 'PembayaranController@detail');
});

Route::prefix('master')->group(function () {
    Route::get('/biaya', 'MasterController@index');
    Route::get('/biaya/edit/{id}', 'MasterController@edit');
    Route::post('/biaya/edit/{id}', 'MasterController@simpan');
    Route::post('/biaya', 'MasterController@tambah');
    Route::get('/biaya/hapus/{id}', 'MasterController@delete');
});

Route::prefix('rekam_medis')->group(function () {
    Route::get('/', 'RekamMedisController@index');
    Route::get('/edit/{id}', 'RekamMedisController@edit');
    Route::post('/simpan/{id}', 'RekamMedisController@simpan');
    Route::get('/rekam/{id}', 'RekamMedisController@rekam');
    Route::get('/imunisasi/{id}/{anak}', 'RekamMedisController@cetakImunisasi');
    Route::get('/kb/{id}', 'RekamMedisController@cetakKb');
    Route::get('/kandungan/{id}', 'RekamMedisController@cetakKandungan');
});

Route::prefix('obat')->group(function () {
    Route::get('/', 'ObatController@index');
    Route::post('/tambah', 'ObatController@tambah');
    Route::get('/edit/{id}', 'ObatController@edit');
    Route::get('/haha/{id}', function($id) {
        return App\Services\Obat\Obat::findOrFail($id);
    });
    Route::post('/simpan/{id}', 'ObatController@editSimpan');
    Route::get('/hapus/{id}', 'ObatController@hapus');

    Route::prefix('pemasukan')->group(function () {
        Route::get('/', 'ObatController@pemasukanindex');
        Route::get('/detail/{id}', 'ObatController@pemasukanread');
        Route::post('/tambah', 'ObatController@pemasukantambah');
    });
    Route::prefix('penggunaan')->group(function () {
        Route::get('/', 'ObatController@penggunaanindex');
        Route::get('/detail/{id}', 'ObatController@penggunaanread');
        Route::post('/tambah', 'ObatController@penggunaantambah');
        Route::post('/edit', 'ObatController@penggunaanedit');
        Route::get('/hapus/{id}', 'ObatController@penggunaanhapus');
    });
    Route::prefix('pengeluaran')->group(function () {
        Route::get('/', 'ObatController@pengeluaranindex');
        Route::get('/detail/{id}', 'ObatController@pengeluaranread');
    });
});

Route::prefix('pasien')->group(function () {
    Route::get('/{id}', function($id) {
        return App\Services\Pasien\Pasien::findOrFail($id);
    });
    Route::get('/tambah', 'PasienController@tambah');
        // Matches The "/admin/users" URL
});

Route::prefix('laporan')->group(function () {
    Route::get('/', function() {
        return view('admin.laporan.index');
    });
    Route::get('/pendaftaran', 'LaporanController@pendaftaran');
    Route::get('/perawatan', 'LaporanController@perawatan');
    Route::get('/pasien', 'LaporanController@pasien');
    Route::get('/perawatan/pasien', 'LaporanController@perawatanpasien');
    Route::get('/pembayaran', 'LaporanController@pembayaran');
    Route::get('/obat', 'LaporanController@obat');
});
