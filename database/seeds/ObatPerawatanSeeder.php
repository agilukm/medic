<?php

use Illuminate\Database\Seeder;

class ObatPerawatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('obat_pemeriksaan')->insert([
            [   'jenis' => 'Kandungan',
                'obat_id' => '1',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Kandungan',
                'obat_id' => '2',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Kandungan',
                'obat_id' => '3',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Lahiran',
                'obat_id' => '4',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Lahiran',
                'obat_id' => '5',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Lahiran',
                'obat_id' => '6',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Imunisasi',
                'obat_id' => '7',
                'jumlah' => '1',
            ],
            [   'jenis' => 'Kb',
                'obat_id' => '8',
                'jumlah' => '1',
            ],
        ]);
    }
}
