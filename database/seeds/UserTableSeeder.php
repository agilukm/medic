<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            [
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'roles' => 1
        ], [
            'name' => 'bidan',
            'email' => 'bidan@bidan.com',
            'password' => bcrypt('bidan'),
            'roles' => 2
        ],
        [
            'name' => 'pemilik',
            'email' => 'pemilik@pemilik.com',
            'password' => bcrypt('pemilik'),
            'roles' => 3
        ]

    ]);
    }
}
