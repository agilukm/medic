<?php

use Illuminate\Database\Seeder;

class MasterDetailPerawatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('data_pemeriksaan')->insert([
            [   'jenis' => 'Kb',
                'detail' => 'Suntik KB',
                'harga' => '30000',
            ], [
                'jenis' => 'Kandungan',
                'detail' => 'Periksa Kandungan',
                'harga' => '60000',
            ], [
                'jenis' => 'Imunisasi',
                'detail' => 'Imunisasi',
                'harga' => '40000',
            ], [
                'jenis' => 'Lahiran',
                'detail' => 'Rawat Inap',
                'harga' => '1000000',
            ]
        ]);
    }
}
