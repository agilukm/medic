<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(MasterDetailPerawatanSeeder::class);
        $this->call(ObatSeeder::class);
        $this->call(ObatPerawatanSeeder::class);
    }
}
