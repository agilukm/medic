<?php

use Illuminate\Database\Seeder;

class ObatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('obat')->insert([
        [
            'id' => 1,
            'nama' => 'Anelat',
            'stok' => '0',
            'satuan' => 'Lempeng'
        ],
        [
            'id' => 2,
            'nama' => 'Calsifar',
            'stok' => '0',
            'satuan' => 'Lempeng'
        ],
        [
            'id' => 3,
            'nama' => 'Samcobion',
            'stok' => '0',
            'satuan' => 'Lempeng'
        ],
        [
            'id' => 4,
            'nama' => 'Etamox 500mg',
            'stok' => '0',
            'satuan' => 'Lempeng'
        ],
        [
            'id' => 5,
            'nama' => 'Fasidol',
            'stok' => '0',
            'satuan' => 'Lempeng'
        ],
        [
            'id' => 6,
            'nama' => 'Vitamin A',
            'stok' => '0',
            'satuan' => 'Lempeng'
        ],
        [
            'id' => 7,
            'nama' => 'Sanmol',
            'stok' => '0',
            'satuan' => 'Botol'
        ],
        [
            'id' => 8,
            'nama' => 'Suntik KB',
            'stok' => '0',
            'satuan' => ''
        ],
    ]);
    }
}
