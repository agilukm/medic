<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTglKembali extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('imunisasi', function (Blueprint $table) {
            $table->date('tgl_kembali');
        });
        Schema::table('kandungan', function (Blueprint $table) {
            $table->date('tgl_kembali');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imunisasi', function (Blueprint $table) {
            $table->dropColumn(['tgl_kembali']);
        });
        Schema::table('kandungan', function (Blueprint $table) {
            $table->dropColumn(['tgl_kembali']);
        });
    }
}
