<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogPerawatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_perawatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pendaftaran_id')->unsigned();
            $table->integer('pasien_id')->unsigned();
            $table->string('jenis');
            $table->string('jenis_id');
            $table->timestamps();
            $table->foreign('pasien_id')
              ->references('id')->on('pasien')
              ->onDelete('cascade');

              $table->foreign('pendaftaran_id')
                ->references('id')->on('registrasi')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_perawatan');
    }
}
