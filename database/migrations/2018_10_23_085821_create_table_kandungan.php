<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKandungan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kandungan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();
            $table->string('anamnesis');
            $table->integer('gpa');
            $table->integer('berat_badan');
            $table->string('tekanan_darah');
            $table->string('tinggi_fundus_uteri');
            $table->string('lingkar_lengan_atas');
            $table->string('denyut_jantung_janin');
            $table->string('jumlah_janin');
            $table->string('presentasi');
            $table->string('status_imunisasi');
            $table->string('tablet_fe');
            $table->timestamps();

            $table->foreign('pasien_id')
              ->references('id')->on('pasien')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kandungan');
    }
}
