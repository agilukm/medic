<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImunisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imunisasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();
            $table->string('nama_bayi');
            $table->string('jenis_kelamin');
            $table->date('tgl_lahir');
            $table->string('umur');
            $table->string('berat_badan');
            $table->string('jenis_imunisasi');
            $table->string('ket');
            $table->timestamps();

            $table->foreign('pasien_id')
              ->references('id')->on('pasien')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imunisasi');
    }
}
