<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObatPerawatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obat_perawatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis');
            $table->integer('obat_id')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();

            $table->foreign('obat_id')
              ->references('id')->on('obat')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
