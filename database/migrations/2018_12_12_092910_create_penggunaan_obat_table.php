<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggunaanObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggunaan_obat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pendaftaran_id')->unsigned();
            $table->integer('obat_id')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();


            $table->foreign('pendaftaran_id')
              ->references('id')->on('registrasi')
              ->onDelete('cascade');

              $table->foreign('obat_id')
                ->references('id')->on('obat')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggunaan_obat');
    }
}
