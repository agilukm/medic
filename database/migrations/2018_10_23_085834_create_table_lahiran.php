<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLahiran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lahiran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();
            $table->integer('umur');
            $table->datetime('jam_lahir');
            $table->string('jenis_kelamin');
            $table->integer('berat_badan');
            $table->string('pb');
            $table->string('therapi');
            $table->timestamps();

            $table->foreign('pasien_id')
              ->references('id')->on('pasien')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lahiran');
    }
}
