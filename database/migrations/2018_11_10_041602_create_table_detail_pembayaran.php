<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pembayaran_id')->unsigned();
            $table->integer('perawatan_id');
            $table->integer('jumlah');
            $table->integer('total');
            $table->timestamps();

            $table->foreign('pembayaran_id')
              ->references('id')->on('pembayaran')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pembayaran');
    }
}
