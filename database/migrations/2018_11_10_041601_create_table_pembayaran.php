<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pendaftaran_id')->unsigned();
            $table->integer('pasien_id')->unsigned();
            $table->integer('jumlah');
            $table->integer('diskon');
            $table->integer('total');
            $table->string('status');
            $table->timestamps();

            $table->foreign('pasien_id')
              ->references('id')->on('pasien')
              ->onDelete('cascade');

              $table->foreign('pendaftaran_id')
                ->references('id')->on('registrasi')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pembayaran');
    }
}
