<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailPembelianObat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pembelian_obat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pembelian_obat_id')->unsigned();
            $table->integer('obat_id')->unsigned();
            $table->integer('jumlah');
            $table->integer('harga');
            $table->integer('total');
            $table->timestamps();

            $table->foreign('pembelian_obat_id')
              ->references('id')->on('pembelian_obat')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pembelian_obat');
    }
}
