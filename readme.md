<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Instalasi

### Composer
Install composer `https://getcomposer.org/Composer-Setup.exe`

Install git `https://github.com/git-for-windows/git/releases/download/v2.18.0.windows.1/Git-2.18.0-64-bit.exe`

1. Masuk ke folder yang sudah di download / extract
2. Klik kanan dan pilih `Git Bash here`
3. Ketik `composer install` di cmd yang muncul
4. Ketik `php artisan key:generate`
5. Copy `.env.example` paste dan reanme menjadi `.env`
6. Edit `.env` file bagian
 - DB_DATABASE=namadatabase
 - DB_PASSWORD=
 - DB_USERNAME=root
7. ketik `php artisan migrate:fresh --seed` di command
8. ketik `php artisan serve`
9. Buka `http://localhost:8000` di browser
10. Login dengan username: `admin@admin.com` dan password: `admin`
