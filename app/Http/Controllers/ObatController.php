<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Obat\ObatService;

class ObatController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ObatService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'data' => $this->service->index(),
        );
        return view('admin.obat.index', $data);
    }

    public function tambah(Request $request)
    {
        $tambah = $this->service->simpan($request);
        if ($tambah) {
            return redirect('/obat')->with('message', 'Data berhasil ditambah');
        }
    }

    public function edit($id)
    {
        $data = array(
            'data' => $this->service->read($id),
        );
        return view('admin.obat.edit', $data);
    }

    public function editSimpan(Request $request, $id)
    {
        $edit = $this->service->edit($request, $id);
        if ($edit) {
            return redirect('/obat')->with('message', 'Data berhasil disimpan');
        }
    }

    public function hapus($id)
    {
        $this->service->delete($id);
        return redirect('/obat')->with('message', 'Data berhasil dihapus');
    }

    public function pemasukanindex()
    {
        $data = array(
            'data' => $this->service->pemasukanindex(),
            'obat' => $this->service->index(),
        );
        return view('admin.obat.pemasukanindex', $data);
    }

    public function pemasukantambah(Request $request)
    {
        $tambah = $this->service->pemasukansimpan($request);
        if ($tambah) {
            return redirect('/obat/pemasukan')->with('message', 'Data dan stok obat berhasil ditambah');
        }
    }

    public function pemasukanread($id)
    {
        return \App\Services\Obat\PemasukanObat::where('pembelian_obat.id', $id)
            ->join('detail_pembelian_obat', 'pembelian_obat_id', '=','pembelian_obat.id')
            ->join('obat', 'obat_id', '=','obat.id')
            ->get();
    }

    public function pengeluaranindex()
    {
        $data = array(
            'data' => $this->service->pengeluaranindex(),
            'obat' => $this->service->index(),
        );
        return view('admin.obat.pengeluaranindex', $data);
    }


    public function penggunaanindex()
    {
        $data = array(
            'data' => $this->service->penggunaanindex(),
            'obat' => $this->service->index(),
        );
        return view('admin.obat.penggunaanindex', $data);
    }

    public function penggunaantambah(Request $request)
    {
        $tambah = $this->service->penggunaansimpan($request);
        if ($tambah) {
            return redirect('/obat/penggunaan')->with('message', 'Data berhasil ditambah');
        }
    }

    public function penggunaanread($id)
    {
        return \App\Services\Obat\ObatPerawatan::select('*', 'obat_pemeriksaan.id as id')->join('obat', 'obat_id', '=','obat.id')->find($id);
    }

    public function penggunaanedit(Request $request)
    {
        $tambah = $this->service->penggunaanedit($request);
        if ($tambah) {
            return redirect('/obat/penggunaan')->with('message', 'Data berhasil disimpan');
        }
    }

    public function penggunaanhapus($id)
    {
        \App\Services\Obat\ObatPerawatan::find($id)->delete();
        return redirect('/obat/penggunaan')->with('message', 'Data berhasil dihapus');

    }
}
