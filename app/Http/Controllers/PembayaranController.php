<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Pembayaran\PembayaranService;

class PembayaranController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PembayaranService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'data' => $this->service->index(),
        );
        return view('admin.pembayaran.index', $data);

    }

    public function readByRegistrasi($id)
    {
        $data = array(
            'registrasi' => $this->service->readByRegistrasi($id),
            'data_pemeriksaan' => $this->service->getDetailPerawatan($id)
        );
        return view('admin.pembayaran.bayar', $data);
    }

    public function detail($id)
    {
        $data = array(
            'registrasi' => $this->service->readByRegistrasi($id),
            'data_pemeriksaan' => $this->service->getDetailPerawatan($id)
        );
        return view('admin.pembayaran.detail', $data);
    }

    public function bayar(Request $request)
    {
        $bayar = $this->service->bayar($request);
        return redirect("/pendaftaran/proses/3/".$bayar->pendaftaran_id)->with('message','Pembayaran Berhasil Dilakukan');
    }
}
