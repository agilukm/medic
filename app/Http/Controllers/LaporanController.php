<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function pendaftaran(Request $request)
    {
        $pendaftaran = \App\Services\Pendaftaran\Pendaftaran::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
        $data = [
            "pendaftarans" => $pendaftaran,
            'awal' => $request->mulai,
            'akhir' => $request->akhir
        ];
        $pdf = \PDF::loadView('admin.laporan.pendaftaran', $data);
        return $pdf->download('pendaftaran '.$request->mulai.' / '.$request->akhir.'.pdf');
    }

    public function perawatan(Request $request)
    {
        if ($request->jenis == 'Imunisasi') {
            $model = \App\Services\Perawatan\Imunisasi\Imunisasi::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.imunisasi';
        }
        else if($request->jenis == 'Kb') {
            $model = \App\Services\Perawatan\Kb\Kb::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.kb';
        }
        else if($request->jenis == 'Kandungan') {
            $model = \App\Services\Perawatan\Kandungan\Kandungan::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.kandungan';
        }
        else {
            $model = \App\Services\Perawatan\Lahiran\Lahiran::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.lahiran';
        }
        $data = [
            "data" => $model,
            'awal' => $request->mulai,
            'akhir' => $request->akhir
        ];
        $pdf = \PDF::loadView($view, $data);
           $pdf->setPaper('A4', 'landscape');
        return $pdf->download('perawatan '.$request->jenis. ' '.$request->mulai.' / '.$request->akhir.'.pdf');
    }

    public function perawatanpasien(Request $request)
    {
        if ($request->jenis == 'Imunisasi') {
            $model = \App\Services\Perawatan\Imunisasi\Imunisasi::where('pasien_id', $request->pasien_id)->whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.imunisasi';
        }
        else if($request->jenis == 'Kb') {
            $model = \App\Services\Perawatan\Kb\Kb::where('pasien_id', $request->pasien_id)->whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.kb';
        }
        else if($request->jenis == 'Kandungan') {
            $model = \App\Services\Perawatan\Kandungan\Kandungan::where('pasien_id', $request->pasien_id)->whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.kandungan';
        }
        else {
            $model = \App\Services\Perawatan\Lahiran\Lahiran::where('pasien_id', $request->pasien_id)->whereBetween('created_at', [$request->mulai, $request->akhir])->get();
            $view = 'admin.laporan.lahiran';
        }
        $data = [
            "data" => $model,
            'awal' => $request->mulai,
            'akhir' => $request->akhir
        ];
        $pdf = \PDF::loadView($view, $data);
        return $pdf->download('perawatan pasien '.$request->jenis. ' '.$request->mulai.' / '.$request->akhir.'.pdf');
    }

    public function pembayaran(Request $request)
    {
        $pembayaran = \App\Services\Pembayaran\Pembayaran::whereBetween('created_at', [$request->mulai, $request->akhir])->get();
        $data = [
            "data" => $pembayaran,
            'awal' => $request->mulai,
            'akhir' => $request->akhir
        ];
        $pdf = \PDF::loadView('admin.laporan.pembayaran', $data);
        return $pdf->download('pembayaran '.$request->mulai.' / '.$request->akhir.'.pdf');
    }

        public function pasien(Request $request)
        {
            $pasien = \App\Services\Pasien\Pasien::get();
            $data = [
                "data" => $pasien
            ];
            $pdf = \PDF::loadView('admin.laporan.pasien', $data);
            return $pdf->download('pasien.pdf');
        }

        public function obat(Request $request)
        {
            $obat = \App\Services\Obat\Obat::get();
            $data = [
                "data" => $obat
            ];
            $pdf = \PDF::loadView('admin.laporan.obat', $data);
            return $pdf->download('obat.pdf');
        }
}
