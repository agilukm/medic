<?php

namespace App\Http\Controllers\Perawatan;

use Illuminate\Http\Request;
use App\Services\Perawatan\Lahiran\LahiranService;
use App\Http\Controllers\Controller;

class LahiranController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LahiranService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'data' => $this->service->index(),
        );
        return view('admin.perawatan.lahiran.index', $data);
    }

    public function readByPasien($pasien_id, $pendaftaran_id)
    {
        $data = array(
            'pendaftaran_id' => $pendaftaran_id,
            'pasien' => \DB::table('pasien')->where('id', $pasien_id)->first(),
            'imunisasi' => $this->service->readByPasien($pasien_id),
            'detail_pasien' => $this->service->detailPasien($pasien_id)
        );
        return view('admin.perawatan.lahiran.detail', $data);
    }

    public function readByPasienAll($pasien_id, $nama_bayi)
    {
        return $this->service->detailPasienAll($pasien_id, $nama_bayi);
    }

    public function tambahById($id, $pendaftaran_id)
    {
        $data = array('imunisasi' => $this->service->read($id), 'pendaftaran_id' => $pendaftaran_id );
        return view('admin.perawatan.lahiran.tambah', $data);
    }

    public function simpan(Request $request)
    {
        $lahiran = $this->service->simpan($request->all());
        return redirect('pembayaran/registrasi/'.$request->pendaftaran_id)->with('message', 'Berhasil Diproses');
    }

    public function pilih($pasien_id)
    {
        $data = array('pasien' => $this->service->readByPasien($pasien_id) );
        return view('admin.perawatan.lahiran.tambahByPasien', $data);
    }
}
