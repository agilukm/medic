<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Pendaftaran\PendaftaranService;

class PendaftaranController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PendaftaranService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'pendaftarans' => $this->service->index()
        );
        return view('admin.pendaftaran.index', $data);
    }

    public function tambah()
    {
        $data = array(
            'pasien' => \DB::table('pasien')->get()
        );
        return view('admin.pendaftaran.tambah', $data);
    }

    public function daftar(Request $request)
    {
        $daftar = $this->service->daftar($request->all());
        if($daftar){
            return redirect('pendaftaran')->with('message', 'Berhasil Disimpan');
        }
    }

    public function proses($status, $id)
    {
        $proses = $this->service->proses($status, $id);
        if ($status == 1) {
            if($proses->perawatan == 'Imunisasi'){
                return redirect('perawatan/'.$proses->perawatan.'/detail/'.$proses->pasien_id. '/'.$proses->id)->with('message', 'Silahkan pilih anak yang akan di imunisasi');
            }
            if($proses->perawatan == 'Lahiran'){
                return redirect('pendaftaran')->with('message', 'Berhasil Disimpan, pasien sedang dirawat.');
            }
            return redirect('perawatan/'.$proses->perawatan.'/detail/'.$proses->pasien_id. '/'.$proses->id)->with('message', 'Silahkan tambah');
        }
        else if($status == 2) {
            return redirect('pembayaran/registrasi/'.$id)->with('message', 'Silahkan lakukan pembayaran');
        }
        else {
            return redirect('pendaftaran')->with('message', 'Berhasil Diproses');
        }
    }

    public function detail($status, $id)
    {
        $proses = $this->service->proses($status, $id);
        if ($status == 1) {
            return redirect('perawatan/'.$proses->perawatan.'/detail/'.$proses->pasien_id. '/'.$proses->id);
        }

    }
}
