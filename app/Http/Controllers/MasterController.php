<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Obat\ObatService;

class MasterController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ObatService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'data' => \DB::table('data_pemeriksaan')->get(),
        );
        return view('admin.master.biaya', $data);
    }

    public function edit($id)
    {
        $data = array(
            'data' => \DB::table('data_pemeriksaan')->find($id),
        );
        return view('admin.master.editbiaya', $data);
    }

    public function tambah(Request $request)
    {
        $data = \DB::table('data_pemeriksaan')->insert([
            "jenis" => $request->jenis_perawatan,
            "detail" => $request->detail,
            "harga" => $request->harga,
        ]);
        if ($data) {
            return redirect('/master/biaya')->with('message', 'Data berhasil ditambah');
        }
    }

    public function simpan($id, Request $request)
    {
        $data = \DB::table('data_pemeriksaan')->where('id',$id)->update([
            "detail" => $request->detail,
            "harga" => $request->harga,
        ]);
        if ($data) {
            return redirect('/master/biaya')->with('message', 'Data berhasil disimpan');
        }
    }

    public function delete($id)
    {
        $data = \DB::table('data_pemeriksaan')->where('id',$id)->delete();
        if ($data) {
            return redirect('/master/biaya')->with('message', 'Data berhasil dihapus');
        }
    }
}
