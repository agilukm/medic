<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Pasien\PasienService;

class RekamMedisController extends Controller
{
    protected $service;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PasienService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array(
            'data' => $this->service->index(),
        );
        return view('admin.rekam_medis.index', $data);
    }

    public function edit($id)
    {
        $data = array(
            'data' => $this->service->read($id),
        );
        return view('admin.rekam_medis.edit', $data);
    }

    public function simpan(Request $request, $id)
    {
        $edit = $this->service->edit($request, $id);
        if ($edit) {
            return redirect('/rekam_medis')->with('message', 'Data berhasil disimpan');
        }
    }

    public function rekam($id)
    {
        $data = array(
            'data' => $this->service->index(),
            'imunisasi' => \App\Services\Perawatan\Imunisasi\Imunisasi::where('pasien_id', $id)->groupBy('nama_bayi')->get(),
            'kandungan' => \App\Services\Perawatan\Kandungan\Kandungan::where('pasien_id', $id)->get(),
            'lahiran' => \App\Services\Perawatan\Lahiran\Lahiran::where('pasien_id', $id)->get(),
            'kb' => \App\Services\Perawatan\Kb\Kb::where('pasien_id', $id)->get(),
        );
        return view('admin.rekam_medis.rekam', $data);
    }

    public function cetakImunisasi($pasien_id, $nama_bayi)
    {
        $imunisasi = \App\Services\Perawatan\Imunisasi\Imunisasi::where('pasien_id', $pasien_id)->where('nama_bayi', $nama_bayi)->groupBy('nama_bayi')->get();
        $data = [
            "data" => $imunisasi,
            "nama" => $nama_bayi,
            "pasien" => \App\Services\Pasien\Pasien::find($pasien_id),
        ];
        $pdf = \PDF::loadView('admin.rekam_medis.imunisasi', $data);
        return $pdf->download('kartu imunisasi '.$nama_bayi.'.pdf');
    }
    public function cetakKb($pasien_id)
    {
        $kb = \App\Services\Perawatan\Kb\Kb::where('pasien_id', $pasien_id)->get();
        $data = [
            "data" => $kb,
            "pasien" => \App\Services\Pasien\Pasien::find($pasien_id),
        ];
        $pdf = \PDF::loadView('admin.rekam_medis.kb', $data);
        return $pdf->download('kartu kb.pdf');
    }
    public function cetakKandungan($pasien_id)
    {
        $kb = \App\Services\Perawatan\Kandungan\Kandungan::where('pasien_id', $pasien_id)->get();
        $data = [
            "data" => $kb,
            "pasien" => \App\Services\Pasien\Pasien::find($pasien_id),
        ];
        $pdf = \PDF::loadView('admin.rekam_medis.kandungan', $data);
        return $pdf->download('kartu periksa kandungan.pdf');
    }
}
