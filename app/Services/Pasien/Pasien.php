<?php

namespace App\Services\Pasien;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pasien extends Model
{
    protected $table = 'pasien';
    protected $guarded = [
        'id'
    ];

    public function imunisasi()
    {
        return $this->hasMany('App\Services\Perawatan\Imunisasi\Imunisasi', 'pasien_id');
    }

    public function kb()
    {
        return $this->hasMany('App\Services\Perawatan\Kb\Kb', 'pasien_id');
    }

    public function kandungan()
    {
        return $this->hasMany('App\Services\Perawatan\Kandungan\Kandungan', 'pasien_id');
    }

    public function lahiran()
    {
        return $this->hasMany('App\Services\Perawatan\Lahiran\Lahiran', 'pasien_id');
    }
}
