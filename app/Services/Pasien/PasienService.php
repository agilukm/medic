<?php

namespace App\Services\Pasien;

use App\Services\Pasien\Pasien;

class PasienService
{

    private $model;
    private $pasien;

    function __construct(Pasien $pasien)
    {
        $this->model = $pasien;
        $this->pasien = $pasien;
    }

    public function index()
    {
        return $this->pasien->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->pasien->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $pasien = $this->pasien->findOrFail($id);
        $pasien->fill($input->toArray());
        $pasien->save();
        return $pasien;
    }
}
