<?php

namespace App\Services\Obat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengeluaranObat extends Model
{
    protected $table = 'penggunaan_obat';
    protected $guarded = [
        'id'
    ];

    public function pendaftaran()
    {
        return $this->belongsTo(\App\Services\Pendaftaran\Pendaftaran::class, 'pendaftaran_id');
    }

    public function obat()
    {
        return $this->belongsTo(Obat::class, 'obat_id');
    }

}
