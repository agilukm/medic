<?php

namespace App\Services\Obat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPemasukanObat extends Model
{
    protected $table = 'detail_pembelian_obat';
    protected $guarded = [
        'id'
    ];

    public function obat()
    {
        return $this->belongsTo(Obat::class, 'obat_id');
    }

}
