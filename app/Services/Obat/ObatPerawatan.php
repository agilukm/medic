<?php

namespace App\Services\Obat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObatPerawatan extends Model
{
    protected $table = 'obat_pemeriksaan';
    protected $guarded = [
        'id'
    ];

    public function obat()
    {
        return $this->belongsTo(Obat::class, 'obat_id');
    }

}
