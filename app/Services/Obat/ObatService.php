<?php

namespace App\Services\Obat;

class ObatService
{

    private $model;
    private $pemasukanmodel;

    function __construct(Obat $model, PemasukanObat $pemasukanmodel, PengeluaranObat $pengeluaranmodel, ObatPerawatan $penggunaanmodel)
    {
        $this->model = $model;
        $this->pemasukanmodel = $pemasukanmodel;
        $this->pengeluaranmodel = $pengeluaranmodel;
        $this->penggunaanmodel = $penggunaanmodel;
    }

    public function index()
    {
        return $this->model->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function edit($input, $id)
    {
        $model = $this->model->findOrFail($id);
        $data = $this->fillObat($model, $input);
        $data->save();
        return $data;
    }

    public function simpan($input)
    {
        $data = $this->fillObat($this->model, $input);
        $data->save();
        return $data;
    }

    public function fillObat($model, $data)
    {
        $model->nama = $data['nama'];
        $model->satuan = $data['satuan'];
        $model->harga_jual = $data['harga_jual'];
        return $model;
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    //pemasukan

    public function pemasukanindex()
    {
        return $this->pemasukanmodel->get();
    }

    public function pemasukanread($id)
    {
        return $this->pemasukanmodel->findOrFail($id);
    }

    public function pemasukansimpan($input)
    {
        $pemasukan = new PemasukanObat;
        $pemasukan->jumlah = 0;
        $pemasukan->total = 0;
        $pemasukan->save();
        foreach ($input['obat_id'] as $key => $value) {
            $detail_pemasukan = new DetailPemasukanObat;
            $total[] = $input['total'][$key];
            $jumlah[] = $input['jumlah'][$key];
            $detail_pemasukan->pembelian_obat_id = $pemasukan->id;
            $detail_pemasukan->obat_id = $input['obat_id'][$key];
            $detail_pemasukan->jumlah = $input['jumlah'][$key];
            $detail_pemasukan->total = $input['total'][$key];
            $detail_pemasukan->save();

            \DB::table('obat')->where('id', $input['obat_id'][$key])->update([
                "stok" =>  \DB::raw('stok + '.$input['jumlah'][$key])
            ]);
        }

        $pemasukan->jumlah = array_sum($jumlah);
        $pemasukan->total = array_sum($total);
        $pemasukan->save();
        return $pemasukan;
    }

    //pengeluaran

    public function pengeluaranindex()
    {
        return $this->pengeluaranmodel->get();
    }

    public function pengeluaranread($id)
    {
        return $this->pengeluaranmodel->findOrFail($id);
    }
    //penggunaan

    public function penggunaanindex()
    {
        return $this->penggunaanmodel->get();
    }

    public function penggunaanread($id)
    {
        return $this->penggunaanmodel->findOrFail($id);
    }

    public function penggunaansimpan($input)
    {
        foreach ($input['obat_id'] as $key => $value) {
            $this->penggunaanmodel->obat_id = $input['obat_id'][$key];
            $this->penggunaanmodel->jenis = $input['jenis'][$key];
            $this->penggunaanmodel->jumlah = $input['jumlah'][$key];
            $this->penggunaanmodel->save();
        }
        return true;
    }

    public function penggunaanedit($input)
    {
        $edit = $this->penggunaanmodel->findOrFail($input['penggunaan_id']);
        $edit->jumlah = $input['jumlah'];
        $edit->save();
        return $edit;
    }
}
