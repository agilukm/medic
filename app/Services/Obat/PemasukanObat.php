<?php

namespace App\Services\Obat;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PemasukanObat extends Model
{
    protected $table = 'pembelian_obat';
    protected $guarded = [
        'id'
    ];

    public function pemasukan()
    {
        return $this->hasMany(DetailPemasukanObat::class, 'pembelian_obat_id');
    }

}
