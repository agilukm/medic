<?php

namespace App\Services\Pendaftaran;

use App\Services\Pasien\Pasien;

class PendaftaranService
{
    private $model;
    private $pasien;

    function __construct(Pendaftaran $model, Pasien $pasien)
    {
        $this->model = $model;
        $this->pasien = $pasien;
    }

    public function index()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function daftar($input)
    {
        if ($input['tipe_pasien'] == "baru") {
            $pasien = $this->fillPasien($this->pasien, $input);
            $pasien->save();
            $this->fillregistrasi($pasien, $input['tipe_perawatan']);
            return $pasien;
        }
        $pasien = $this->pasien->findOrFail($input['pasien_id']);
        $this->fillregistrasi($pasien, $input['tipe_perawatan']);
        return $pasien;
    }

    public function fillPasien($pasien, $input)
    {
        $maxkode = $this->pasien->count();
        $pasien->kode_pasien = 'P'.++$maxkode;
        $pasien->nama_pasien = $input['nama'];
        $pasien->nama_suami = $input['nama_suami'];
        $pasien->alamat = $input['alamat'];
        $pasien->ktp = $input['ktp'];
        $pasien->tgl_lahir = $input['tgl_lahir'];
        return $pasien;
    }

    public function fillregistrasi($pasien, $tipe_perawatan)
    {
        $year = date('Y');
        $month = date('m');
        $maxkode = $this->model->count();
        $this->model->kode_registrasi = 'REG-'.$year.'-'.$month.'-'.date('d').'-00'.++$maxkode;
        $this->model->pasien_id = $pasien->id;
        $this->model->perawatan = $tipe_perawatan;
        $this->model->waktu_daftar =  \Carbon\Carbon::now()->toDateTimeString();
        $this->model->waktu_masuk = null;
        $this->model->waktu_keluar = null;
        $this->model->status = 0;
        $this->model->save();
        return $this->model;
    }

    public function proses($status, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->status = $status;
        if($status == 1)
        {
            $model->waktu_masuk = \Carbon\Carbon::now()->toDateTimeString();
        }
        else
        {
            $model->waktu_keluar = \Carbon\Carbon::now()->toDateTimeString();
        }
        $model->save();
        return $model;
    }
}
