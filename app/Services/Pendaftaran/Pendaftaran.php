<?php

namespace App\Services\Pendaftaran;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pendaftaran extends Model
{
    protected $table = 'registrasi';
    protected $guarded = [
        'id'
    ];

    const STATUS_WAITING = 0;
    const STATUS_TREATMENT = 1;
    const STATUS_PEMBAYARAN = 2;
    const STATUS_DONE = 3;

    public static function listStatus()
    {
       return [
           self::STATUS_WAITING    => 'Menunggu',
           self::STATUS_TREATMENT => 'Dalam Perawatan',
           self::STATUS_PEMBAYARAN => 'Proses Pembayaran',
           self::STATUS_DONE  => 'Selesai',
       ];
    }

    public function statusLabel()
    {
       $list = self::listStatus();
       return isset($list[$this->status]) ? $list[$this->status] : $this->status;
    }

    public function pasien()
    {
        return $this->belongsTo('App\Services\Pasien\Pasien', 'pasien_id');
    }

    public function pembayaran()
    {
        return $this->hasOne(\App\Services\Pembayaran\Pembayaran::class, 'pendaftaran_id');
    }

}
