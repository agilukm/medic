<?php

namespace App\Services\Perawatan\Kandungan;

use App\Services\Pasien\Pasien;

class KandunganService
{

    private $model;
    private $pasien;

    function __construct(Kandungan $model, Pasien $pasien)
    {
        $this->model = $model;
        $this->pasien = $pasien;
    }

    public function index()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function readByPasien($id)
    {
        return $this->model->where('pasien_id', $id)->first();
    }

    public function detailPasien($id)
    {
        return $this->model->where('pasien_id', $id)->orderBy('created_at')->get();
    }

    public function detailPasienAll($id, $nama_bayi)
    {
        return $this->model->where('pasien_id', $id)->orderBy('created_at')->get();
    }

    public function simpan($input)
    {
        $jumlah = [];
        $imunisasi = $this->fillKandungan($this->model, $input);
        $imunisasi->save();
        $imunisasi->log()->attach([$this->model->id => ['pasien_id' => $input['pasien_id'], 'pendaftaran_id'=> $input['pendaftaran_id'], 'jenis' => 'Kandungan']]);
        \DB::table('registrasi')
            ->where('id', $input['pendaftaran_id'])
            ->update(['status' => 2]);
        $data_pemeriksaan = \DB::table('data_pemeriksaan')->where('jenis', 'Kandungan')->get();

    if (isset($input['obat_id'])) {
        foreach ($input['obat_id'] as $keyz => $ob) {
            $penggunaan_obat = \DB::table('obat_pemeriksaan')->join('obat', 'obat.id', '=', 'obat_pemeriksaan.obat_id')->where('obat_id', $ob)->get();
                foreach($penggunaan_obat as $key => $obat){
                \DB::table('penggunaan_obat')->insert([
                    "pendaftaran_id" => $input['pendaftaran_id'],
                    "obat_id" => $obat->obat_id,
                    "dosis" => $input['dosis'][$obat->obat_id],
                    "jumlah" => $obat->jumlah,
                    "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
               "updated_at" => \Carbon\Carbon::now(),
                ]);
                \DB::table('obat')->where('id', $obat->obat_id)->update([
                    "stok" =>  \DB::raw('stok - '.$obat->jumlah)
                ]);
                $jumlah[] = $obat->harga_jual*$obat->jumlah;
            }
        }
            foreach($data_pemeriksaan as $pera){
                $jumlah[] = $pera->harga;
            }
        }

        $pembayaran = \App\Services\Pembayaran\Pembayaran::create([
            "pendaftaran_id" => $input['pendaftaran_id'],
            "pasien_id" => $input['pasien_id'],
            "jumlah" => array_sum($jumlah),
            "diskon" => 0,
            "total" => array_sum($jumlah),
            "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
           "updated_at" => \Carbon\Carbon::now(),
        ]);

        foreach ($data_pemeriksaan as $key => $value) {
            \DB::table('detail_pembayaran')->insert([
                "pembayaran_id" => $pembayaran->id,
                "perawatan_id" => $value->id,
                "jumlah" => 1,
                "total" => $value->harga,
                "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
           "updated_at" => \Carbon\Carbon::now(),
            ]);
        }

        return $imunisasi;
    }

    public function fillKandungan($model, $input)
    {
        $model->pasien_id = $input['pasien_id'];
        $model->anamnesis = $input['anamnesis'];
        $model->gpa = $input['gpa'];
        $model->berat_badan = $input['berat_badan'];
        $model->tekanan_darah = $input['tekanan_darah'];
        $model->tinggi_fundus_uteri = $input['tinggi_fundus_uteri'];
        $model->lingkar_lengan_atas = $input['lingkar_lengan_atas'];
        $model->denyut_jantung_janin = $input['denyut_jantung_janin'];
        $model->jumlah_janin = $input['jumlah_janin'];
        $model->presentasi = $input['presentasi'];
        $model->status_imunisasi = $input['status_imunisasi'];
        $model->tablet_fe = $input['tablet_fe'];
        $model->tgl_kembali = $input['tgl_kembali'];
        $model->save();
        return $model;
    }

}
