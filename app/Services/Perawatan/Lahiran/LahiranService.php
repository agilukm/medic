<?php

namespace App\Services\Perawatan\Lahiran;

use App\Services\Pasien\Pasien;

class LahiranService
{

    private $model;
    private $pasien;

    function __construct(Lahiran $model, Pasien $pasien)
    {
        $this->model = $model;
        $this->pasien = $pasien;
    }

    public function index()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function readByPasien($id)
    {
        return $this->model->where('pasien_id', $id)->first();
    }

    public function detailPasien($id)
    {
        return $this->model->where('pasien_id', $id)->orderBy('created_at')->get();
    }

    public function detailPasienAll($id)
    {
        return $this->model->where('pasien_id', $id)->orderBy('created_at')->get();
    }

    public function simpan($input)
    {
        $kb = $this->fillLahiran($this->model, $input);
        $kb->save();
        $kb->log()->attach([$this->model->id => ['pasien_id' => $input['pasien_id'], 'pendaftaran_id'=> $input['pendaftaran_id'], 'jenis' => 'Lahiran']]);
        \DB::table('registrasi')
            ->where('id', $input['pendaftaran_id'])
            ->update(['status' => 2, 'waktu_keluar' => \Carbon\Carbon::now()]);
        $penggunaan_obat = \DB::table('obat_pemeriksaan')->where('jenis', 'Lahiran')->get();
        $data_pemeriksaan = \DB::table('data_pemeriksaan')->where('jenis', 'Lahiran')->get();

if (isset($input['obat_id'])) {
        foreach ($input['obat_id'] as $keyz => $ob) {
            $penggunaan_obat = \DB::table('obat_pemeriksaan')->join('obat', 'obat.id', '=', 'obat_pemeriksaan.obat_id')->where('obat_id', $ob)->get();
            foreach($penggunaan_obat as $key => $obat){
                \DB::table('penggunaan_obat')->insert([
                    "pendaftaran_id" => $input['pendaftaran_id'],
                    "obat_id" => $obat->obat_id,
                    "jumlah" => $obat->jumlah,
                    "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
               "updated_at" => \Carbon\Carbon::now(),
                ]);
                \DB::table('obat')->where('id', $obat->obat_id)->update([
                    "stok" =>  \DB::raw('stok - '.$obat->jumlah)
                ]);
                $jumlah[] = $obat->harga_jual*$obat->jumlah;
            }
        }
        }

        foreach($data_pemeriksaan as $pera){
            $jumlah[] = $pera->harga;
        }

        $pembayaran = \App\Services\Pembayaran\Pembayaran::create([
            "pendaftaran_id" => $input['pendaftaran_id'],
            "pasien_id" => $input['pasien_id'],
            "jumlah" => array_sum($jumlah),
            "diskon" => 0,
            "total" => array_sum($jumlah),
            "status" => 0,
            "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
           "updated_at" => \Carbon\Carbon::now(),
        ]);

        foreach ($data_pemeriksaan as $key => $value) {
            \DB::table('detail_pembayaran')->insert([
                "pembayaran_id" => $pembayaran->id,
                "perawatan_id" => $value->id,
                "jumlah" => 1,
                "total" => $value->harga,
                "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
           "updated_at" => \Carbon\Carbon::now(),
            ]);
        }

        return $kb;
    }

    public function fillLahiran($model, $input)
    {
        $model->pasien_id = $input['pasien_id'];
        $model->umur = isset($input['umur']) ? $input['umur'] : 1;
        $model->jam_lahir = $input['jam_lahir'];
        $model->jenis_kelamin = $input['jenis_kelamin'];
        $model->berat_badan = isset($input['berat_badan']) ? $input['berat_badan'] : 1;
        $model->pb = isset($input['pb']) ? $input['pb'] : 1;
        $model->therapi = isset($input['therapi']) ? $input['therapi'] : 1;
        $model->save();
        return $model;
    }

}
