<?php

namespace App\Services\Perawatan\Kb;

use App\Services\Pasien\Pasien;

class KbService
{

    private $model;
    private $pasien;

    function __construct(Kb $model, Pasien $pasien)
    {
        $this->model = $model;
        $this->pasien = $pasien;
    }

    public function index()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function readByPasien($id)
    {
        return $this->model->where('pasien_id', $id)->first();
    }

    public function detailPasien($id)
    {
        return $this->model->where('pasien_id', $id)->orderBy('created_at')->get();
    }

    public function detailPasienAll($id)
    {
        return $this->model->where('pasien_id', $id)->orderBy('created_at')->get();
    }

    public function simpan($input)
    {
        $kb = $this->fillKb($this->model, $input);
        $kb->save();
        $kb->log()->attach([$this->model->id => ['pasien_id' => $input['pasien_id'], 'pendaftaran_id'=> $input['pendaftaran_id'], 'jenis' => 'Kb']]);
        \DB::table('registrasi')
            ->where('id', $input['pendaftaran_id'])
            ->update(['status' => 2,  'waktu_keluar' => \Carbon\Carbon::now()]);
        $data_pemeriksaan = \DB::table('data_pemeriksaan')->where('jenis', 'Kb')->get();

        if (isset($input['obat_id'])) {
            foreach ($input['obat_id'] as $keyz => $ob) {
                $penggunaan_obat = \DB::table('obat_pemeriksaan')->join('obat', 'obat.id', '=', 'obat_pemeriksaan.obat_id')->where('obat_id', $ob)->get();
                foreach($penggunaan_obat as $key => $obat){
                    \DB::table('penggunaan_obat')->insert([
                        "pendaftaran_id" => $input['pendaftaran_id'],
                        "obat_id" => $obat->obat_id,
                        "jumlah" => $obat->jumlah,
                        "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
                        "dosis" =>  $input['dosis'][$obat->obat_id],
                       "updated_at" => \Carbon\Carbon::now(),
                    ]);
                    \DB::table('obat')->where('id', $obat->obat_id)->update([
                        "stok" =>  \DB::raw('stok - '.$obat->jumlah)
                    ]);
                    $jumlah[] = $obat->harga_jual*$obat->jumlah;
                }
            }
        }

        foreach($data_pemeriksaan as $pera){
            $jumlah[] = $pera->harga;
        }

        $pembayaran = \App\Services\Pembayaran\Pembayaran::create([
            "pendaftaran_id" => $input['pendaftaran_id'],
            "pasien_id" => $input['pasien_id'],
            "jumlah" => array_sum($jumlah),
            "diskon" => 0,
            "total" => array_sum($jumlah),
            "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
           "updated_at" => \Carbon\Carbon::now(),
        ]);

        foreach ($data_pemeriksaan as $key => $value) {
            \DB::table('detail_pembayaran')->insert([
                "pembayaran_id" => $pembayaran->id,
                "perawatan_id" => $value->id,
                "jumlah" => 1,
                "total" => $value->harga,
                "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
           "updated_at" => \Carbon\Carbon::now(),
            ]);
        }

        return $kb;
    }

    public function fillKb($model, $input)
    {
        if($input['tipe'] == '1 bulan')
        {
            $tanggal_kembali = date('Y-m-d', strtotime("+1 month"));
        }
        $tanggal_kembali = date('Y-m-d', strtotime("+1 months"));
        $model->pasien_id = $input['pasien_id'];
        $model->berat_badan = $input['berat_badan'];
        $model->tanggal_kembali = $tanggal_kembali;
        $model->tipe = $input['tipe'];
        $model->tekanan_darah = isset($input['tekanan_darah']) ? $input['tekanan_darah'] : 1;
        $model->save();
        return $model;
    }

}
