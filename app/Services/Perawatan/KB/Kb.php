<?php

namespace App\Services\Perawatan\Kb;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kb extends Model
{
    protected $table = 'kb';
    protected $guarded = [
        'id'
    ];

    public function log() {
        return $this->belongsToMany(
             'App\Services\Pendaftaran\Pendaftaran',
             'log_pemeriksaan',
             'jenis_id',
             'pendaftaran_id'
        )->withTimestamps();
    }

    public function pasien() {
        return $this->belongsTo('App\Services\Pasien\Pasien', 'pasien_id');
    }
}
