<?php

namespace App\Services\Perawatan\Imunisasi;

use Illuminate\Database\Eloquent\Model;

class Imunisasi extends Model
{
    protected $table = 'imunisasi';
    protected $guarded = [
        'id'
    ];

    public function log() {
        return $this->belongsToMany(
             'App\Services\Pendaftaran\Pendaftaran',
             'log_pemeriksaan',
             'jenis_id',
             'pendaftaran_id'
        )->withTimestamps();
    }

    public function pasien() {
        return $this->belongsTo('App\Services\Pasien\Pasien', 'pasien_id');
    }
}
