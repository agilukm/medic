<?php

namespace App\Services\Perawatan\Imunisasi;

use App\Services\Pasien\Pasien;

class ImunisasiService
{

    private $model;
    private $pasien;

    function __construct(Imunisasi $model, Pasien $pasien)
    {
        $this->model = $model;
        $this->pasien = $pasien;
    }

    public function browse()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function readByPasien($id)
    {
        return $this->model->where('pasien_id', $id)->first();
    }

    public function detailPasien($id)
    {
        return $this->model->where('pasien_id', $id)->groupBy('nama_bayi')->orderBy('created_at')->get();
    }

    public function detailPasienAll($id, $nama_bayi)
    {
        return $this->model->where('pasien_id', $id)->where('nama_bayi', $nama_bayi)->orderBy('created_at')->get();
    }

    public function simpan($input)
    {
        $imunisasi = $this->fillImunisasi($this->model, $input);
        $imunisasi->save();
        $imunisasi->log()->attach([$this->model->id => ['pasien_id' => $input['pasien_id'], 'pendaftaran_id' => $input['pendaftaran_id'], 'jenis' => 'Imunisasi']]);
        \DB::table('registrasi')
            ->where('id', $input['pendaftaran_id'])
            ->update(['status' => 2, 'waktu_keluar' => \Carbon\Carbon::now()]);

        $data_pemeriksaan = \DB::table('data_pemeriksaan')->where('jenis', 'Imunisasi')->get();

        if (isset($input['obat_id'])) {
        foreach ($input['obat_id'] as $keyz => $ob) {
            $penggunaan_obat = \DB::table('obat_pemeriksaan')->join('obat', 'obat.id', '=', 'obat_pemeriksaan.obat_id')->where('obat_id', $ob)->get();
            foreach ($penggunaan_obat as $key => $obat) {
                \DB::table('penggunaan_obat')->insert([
                    "pendaftaran_id" => $input['pendaftaran_id'],
                    "obat_id" => $obat->obat_id,
                    "jumlah" => $obat->jumlah,
                    "dosis" => $input['dosis'][$obat->obat_id],
                    "created_at" => \Carbon\Carbon::now(), # \Datetime()
                    "updated_at" => \Carbon\Carbon::now(),
                ]);
                \DB::table('obat')->where('id', $obat->obat_id)->update([
                    "stok" => \DB::raw('stok - ' . $obat->jumlah)
                ]);
                $jumlah[] = $obat->harga_jual*$obat->jumlah;
            }
        }
        }

        foreach ($data_pemeriksaan as $pera) {
            $jumlah[] = $pera->harga;
        }

        $pembayaran = \App\Services\Pembayaran\Pembayaran::create([
            "pendaftaran_id" => $input['pendaftaran_id'],
            "pasien_id" => $input['pasien_id'],
            "jumlah" => array_sum($jumlah),
            "diskon" => 0,
            "total" => array_sum($jumlah),
            "created_at" => \Carbon\Carbon::now(), # \Datetime()
            "updated_at" => \Carbon\Carbon::now(),
        ]);

        foreach ($data_pemeriksaan as $key => $value) {
            \DB::table('detail_pembayaran')->insert([
                "pembayaran_id" => $pembayaran->id,
                "perawatan_id" => $value->id,
                "jumlah" => 1,
                "total" => $value->harga,
                "created_at" => \Carbon\Carbon::now(), # \Datetime()
                "updated_at" => \Carbon\Carbon::now(),
            ]);
        }


        return $imunisasi;
    }

    public function fillImunisasi($model, $input)
    {
        $model->pasien_id = $input['pasien_id'];
        $model->nama_bayi = $input['nama_bayi'];
        $model->jenis_kelamin = $input['jenis_kelamin'];
        $model->tgl_lahir = $input['tgl_lahir'];
        $model->umur = isset($input['umur']) ? $input['umur'] : 1;
        $model->berat_badan = isset($input['berat_badan']) ? $input['berat_badan'] : 10;
        $model->jenis_imunisasi = $input['jenis_imunisasi'];
        $model->tgl_kembali = $input['tgl_kembali'];
        $model->ket = isset($input['ket']) ? $input['ket'] : '';
        $model->save();
        return $model;
    }

}
