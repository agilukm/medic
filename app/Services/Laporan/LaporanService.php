<?php

namespace App\Services\Laporan;

use App\Services\Pendaftaran\Pendaftaran;
use App\Services\Pasien\Pasien;

class PendaftaranService
{
    private $model;
    private $pasien;

    function __construct(Pendaftaran $model, Pasien $pasien)
    {
        $this->model = $model;
        $this->pasien = $pasien;
    }

    public function index()
    {
        return $this->model->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

}
