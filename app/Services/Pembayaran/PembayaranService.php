<?php

namespace App\Services\Pembayaran;

use App\Services\Pendaftaran\Pendaftaran;

class PembayaranService
{
    private $model;
    private $pasien;

    function __construct(Pembayaran $model, Pendaftaran $pendaftaran)
    {
        $this->model = $model;
        $this->pendaftaran = $pendaftaran;
    }

    public function index()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function read($id)
    {
        return $this->model->findOrFail($id);
    }

    public function readByRegistrasi($id)
    {
        return $this->pendaftaran->findOrFail($id);
    }

    public function bayar($input)
    {
        $pembayaran = $this->model->where('pendaftaran_id', $input['pendaftaran_id'])->first();
        $bayar = $this->fillPembayaran($pembayaran, $input);
        $bayar->save();
        return $bayar;
    }

    public function getDetailPerawatan($id)
    {
        $pendaftaran = new \App\Services\Pendaftaran\Pendaftaran;
        $daftar = $pendaftaran->where('id', $id)->first();
        return $data_pemeriksaan = \DB::table('data_pemeriksaan')
            ->where('jenis', $daftar->perawatan)->get();
    }

    public function fillPembayaran($pembayaran, $input)
    {
        $pembayaran->pasien_id = $input['pasien_id'];
        $pembayaran->diskon = $input['diskon'];
        $pembayaran->total = $input['tototo'];
        $pembayaran->status = 1;
        return $pembayaran;
    }
}
