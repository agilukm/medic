<?php

namespace App\Services\Pembayaran;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    protected $guarded = [
        'id'
    ];

    public function detail()
    {
      $this->hasMany(DetailPembayaran::class, 'pembayaran_id');
    }

    public function pendaftaran()
    {
        return $this->belongsTo(\App\Services\Pendaftaran\Pendaftaran::class, 'pendaftaran_id');
    }
}
