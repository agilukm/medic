<?php

namespace App\Services\DetailPembayaran;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPembayaran extends Model
{
    protected $table = 'detail_pembayaran';
    protected $guarded = [
        'id'
    ];

    public function pembayaran()
    {
        $this->belongsTo(Pembayaran::class, 'pembayaran_id');
    }
}
